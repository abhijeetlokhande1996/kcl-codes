from sklearn.decomposition import PCA, sparse_encode
from sklearn.datasets import load_iris
import numpy as np
import math


def pca_sklearn():

    pca = PCA(n_components=2)
    pca.fit(load_iris()["data"])
    iris_pca = pca.transform(load_iris()["data"])
    iris_pca = np.round(iris_pca, 3)
    # eigen values = pca.explained_variance_
    # eigen vectors = pca.components_
    # print(X @ pca.components_.T)

    # print("PCA components: \n", pca.components_)
    iris = load_iris()["data"]
    iris_mean = np.mean(iris, axis=0)
    iris_prime = iris - iris_mean
    projections = np.round(iris_prime @ pca.components_.T, 3)
    # print("is it matched")
    # print(iris_pca[: 5])
    # print()
    # print(projections[: 5])
    # print()
    x = np.array([[4.9, 3.0, 6.5, 1.5], [5.7, 4.2, 2.7, 1.9], [
                 5.9, 2.8, 6.2, 0.3], [6.1, 2.2, 4.8, 1.6], [5.7, 3.6, 4.5, 0.4]])
    print(pca.transform(x))
    print()
    x_prime = x - iris_mean
    x_pca = x_prime @ pca.components_.T
    print(x_pca)
    print()
    Z = np.array([[7.9, 3.2, 5.1, 2.0], [5.2, 3.9, 4.3, 1.1], [
                 4.7, 2.7, 6.2, 0.6], [4.7, 3.5, 5.9, 2.2], [7.1, 3.3, 6.0, 2.1]])
    print(pca.transform(Z))
    Z_prime = Z - iris_mean
    print()
    print(Z_prime @ pca.components_.T)


def sparse_coding():
    numNonZero = 2
    tolerance = 1.000000e-05
    iris = load_iris()["data"]
    target = load_iris()["target"]
    dict_1 = iris[target == 0]
    dict_2 = iris[target == 1]
    dict_3 = iris[target == 2]
    _lambda = 0.1

    # x = np.array([[4.8, 3.8, 6.4, 1.5], [6.7, 2.7, 3.5, 0.7], [
    #              4.8, 2.2, 2.1, 2.2], [7.3, 2.8, 4.2, 1.1], [7.2, 2.2, 6.1, 2.5]])

    x = np.array([[6.2, 2.3, 2.6, 0.1], [5.7, 4.1, 2.7, 1.0], [
                 4.6, 3.6, 6.1, 2.2], [7.5, 2.7, 4.1, 1.8], [7.6, 3.0, 5.2, 0.5]])
    # Geoge ip
    # x = np.array([[5.1, 2.4, 3.8, 1.3], [5.9, 3.3, 6.5, 2.4], [
    #          4.8, 4.1, 6.2, 0.4], [4.8, 3.4, 5.9, 0.5], [7.8, 3.3, 5.8, 0.6]])

    for idx, ip in enumerate(x):
        ip = ip.reshape(1, -1)
        tmp = []
        for idy, d in enumerate([dict_1, dict_2, dict_3]):
            V = d
            y = sparse_encode(ip, V, algorithm='omp',
                              n_nonzero_coefs=numNonZero, alpha=tolerance)
            first_term = np.power(np.sum((ip - y @ V) ** 2), 0.5)
            second_term = _lambda * len(y[y != 0])
            cost = first_term + second_term
            print(f"Cost of input {idx + 1} using Dict {idy + 1} is ", cost)
            tmp.append(cost)
        print(f"class of sample_{idx + 1}", np.argmin(tmp))
        print()


def test():
    x = np.array([[-0.05, -0.95]])
    V = np.array([[0.4, 0.55, 0.5, -0.1, -0.5, 0.9, 0.5, 0.45],
                  [-0.6, -0.45, -0.5, 0.9, -0.5, 0.1, 0.5, 0.55]])
    # y = np.array([1, 0, 0, 0, 1, 0, 0, 0]).reshape(-1, 1)
    y = np.array([0, 0, 1, 0, 0, 0, -1, 0]).reshape(-1)
    # _lambda = 1
    first_term = ((x - y.T @ V.T) ** 2).reshape(-1)
    ans = math.sqrt(sum(first_term))
    print(ans)


pca_sklearn()
sparse_coding()
# test()
