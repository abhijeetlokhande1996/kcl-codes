"""
	Delta Rule: Unthreshold Perceptron used in Linear Regression
	Generalised Perceptron: Sigmoid or any soft activation function
"""

# delta rule is nothing but unthreshold perceptron
# it is used in Linear Regression

import numpy as np
from scipy.special import expit
# expit -> sigmoid function

# delta rule start

x = np.array([[1, 2], [3, 1], [1, 1], [2, 0]])
y = np.array([1, 0, 1, 0])
w = np.array([1, 2])

eta = 0.5
# delta rule
# in this rule we sum of square residul cost function


epoch = 1
for _ in range(epoch):
    for idx, each_x in enumerate(x):
        t = y[idx]
        op = w @ each_x
        w = w + eta * (t - op) * each_x

print("Weight using Delta Rule: ", w)
# delta rule end

# generalised delta rule start


def sigmoid(x, deriv=False):
    if deriv:
        return x * (1 - x)
    return 1/(1+np.exp(-x))


x = np.array([[1, 2], [3, 1], [1, 1], [2, 0]])
y = np.array([1, 0, 1, 0])
w = np.array([1, 2])

eta = 0.5
epoch = 1
for _ in range(epoch):
    for idx, each_x in enumerate(x):
        t = y[idx]
        op = sigmoid(w @ each_x)
        error = t - op
        error_delta = error * sigmoid(op, deriv=True)
        w = w + eta * error_delta * each_x


print("Weight using Generalised Delta Rule: ", w)

# generalised delta rule end
