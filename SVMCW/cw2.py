import pandas as pd
from copy import deepcopy
import numpy as np
import matplotlib.pyplot as plt

from sklearn.svm import SVC
import seaborn as sns
from mlxtend.plotting import plot_decision_regions
sns.set()


def sklearn_plot_support_vector(X, y):
    y_copy = deepcopy(y)

    # for idx, el in enumerate(y_copy):
    #     if el == (i+1):
    #         y[idx] = 1
    #     else:
    #         y[idx] = -1

    X = X[0:21, :]
    y = y[0:21]
    y = np.where(y == 1, 1, -1)

    model = SVC(kernel='linear', C=1E10)
    model.fit(X, y)
    ax = plt.gca()
    sns.scatterplot(X[:, 0], X[:, 1], hue=y, s=50,
                    palette=["red", "green"])
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()

    xx = np.linspace(xlim[0], xlim[1], 30)
    yy = np.linspace(ylim[0], ylim[1], 30)
    YY, XX = np.meshgrid(yy, xx)
    xy = np.vstack([XX.ravel(), YY.ravel()]).T
    Z = model.decision_function(xy).reshape(XX.shape)

    ax.contour(XX, YY, Z, colors='k', levels=[-1, 0, 1], alpha=0.5,
               linestyles=['--', '-', '--'])

    ax.scatter(model.support_vectors_[:, 0], model.support_vectors_[:, 1], s=100,
               linewidth=1, facecolors='none', edgecolors='k')
    plt.title(f"'Class 1 VS Class 2'")
    plt.savefig(f"./'Class 1 VS Class 2'.jpeg")
    plt.show()


def plotting_support_vector(X, Y):
    clf = SVC(decision_function_shape='ovo')
    clf.fit(X, Y)

    # Plot Decision Region using mlxtend's awesome plotting function
    plot_decision_regions(X=X, y=Y, clf=clf, legend=2)

    # Update plot object with X/Y axis labels and Figure Title
    plt.xlabel("Feature 1", size=14)
    plt.ylabel("Feature 2", size=14)
    plt.title('SVM Decision Region Boundary', size=16)
    plt.show()


def plot_decision_boundary(X, y):
    def make_meshgrid(x, y, h=.02):
        x_min, x_max = x.min() - 1, x.max() + 1
        y_min, y_max = y.min() - 1, y.max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                             np.arange(y_min, y_max, h))
        return xx, yy

    def plot_contours(ax, clf, xx, yy, **params):
        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)
        out = ax.contourf(xx, yy, Z, **params)
        return out

    model = SVC(kernel="linear")
    clf = model.fit(X, y)

    fig, ax = plt.subplots()

    # Set-up grid for plotting.
    X0, X1 = X[:, 0], X[:, 1]
    xx, yy = make_meshgrid(X0, X1)
    title = "Decision Boundary Combined"
    plot_contours(ax, clf, xx, yy, cmap=plt.cm.coolwarm, alpha=0.8)
    ax.scatter(X0, X1, c=y, cmap=plt.cm.coolwarm, s=20, edgecolors="k")
    ax.set_ylabel("{}".format("Feature 1"))
    ax.set_xlabel("{}".format("Feature 2"))
    ax.set_xticks(())
    ax.set_yticks(())
    ax.set_title(title)
    plt.savefig("./DecisionBoundaryCombine.jpeg")
    plt.show()


def Q7(X, y, test):
    print(test)
    y_copy = deepcopy(y)
    # first detect sample whether it label as class 3 or not
    w_1 = np.array([0.2405, 0.253]).reshape(-1, 1)
    b_1 = -0.857
    for ip in test:
        ip = ip.reshape(-1, 1)
        pred = (w_1.T @ ip + b_1)
        pred = pred.reshape(-1)
        pred = pred[0]
        print(pred, 1 if pred > 0 else -1)
        break


if __name__ == "__main__":
    x1 = np.array([[1, 1], [1.5, 1.5], [2, 2], [1, 1.5], [0.5, 0.5], [
        1.5, 1], [0.5, 1], [1, 0.5], [0.5, 1.5], [1.5, 0.5]])

    x2 = np.array([[6, 2], [6.5, 0.5], [7, 0.5], [7.5, 0.5], [6.5, 1], [
        7, 1], [7.5, 1], [6.5, 1.5], [7, 1.5], [7.5, 1.5]])

    x3 = np.array([[3.5, 4], [4, 4.5], [4, 5], [4.5, 5], [5, 5], [
        4.5, 4.5], [5, 4.5], [4, 5.5], [4.5, 5.5], [5.5, 5.5]])

    X = np.concatenate((x1, x2, x3))
    y = np.concatenate(([1] * 10, [2] * 10, [3] * 10))
    new_sample = []
    for i in range(x1.shape[0]):
        sx1 = x1[i][0]
        sx2 = x2[i][0]
        sx3 = x3[i][0]

        sy1 = x1[i][1]
        sy2 = x2[i][1]
        sy3 = x3[i][1]

        new_sample.append([(sx1 + sx2 + sx3) / 3, (sy1 + sy2 + sy3) / 3])
    new_sample = np.array(new_sample)
    Q7(X, y, new_sample)

    # df = pd.DataFrame({"Sample of Class 1": list(map(lambda x: str(x.tolist()), x1)),
    #                    "Sample of Class 2": list(map(lambda x: str(x.tolist()), x2)), "Sample of Class 3": list(map(lambda x: str(x.tolist()), x3))}, index=range(x1.shape[0]))
    # df.to_excel("./samples.xlsx")
    # sns.scatterplot(X[:, 0], X[:, 1], hue=y, palette=["red", "green", "blue"])
    # plt.savefig("./Dataset.jpeg")
    # plt.show()
    # plot_decision_boundary(X, y)
    # plotting_support_vector(X, y)
    # sklearn_plot_support_vector(X, y)
