from sklearn.svm import SVC
from mlxtend.plotting import plot_decision_regions
from numpy.lib.npyio import load
from sklearn import svm
import matplotlib.pyplot as plt
import pandas as pd
from sys import platform
from sklearn.datasets import make_classification
import numpy as np
from matplotlib import pyplot as plt

import seaborn as sns
sns.set()


def plotting_support_vector(X, Y):
    clf = svm.SVC(decision_function_shape='ovo')
    clf.fit(X, Y)

    # Plot Decision Region using mlxtend's awesome plotting function
    plot_decision_regions(X=X, y=Y, clf=clf, legend=2)

    # Update plot object with X/Y axis labels and Figure Title
    plt.xlabel("Feature 1", size=14)
    plt.ylabel("Feature 2", size=14)
    plt.title('SVM Decision Region Boundary', size=16)
    plt.show()


def make_meshgrid(x, y, h=.02):
    x_min, x_max = x.min() - 1, x.max() + 1
    y_min, y_max = y.min() - 1, y.max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    return xx, yy


def plot_contours(ax, clf, xx, yy, **params):
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    out = ax.contourf(xx, yy, Z, **params)
    return out


# for i in range(10):
# 10000
X, y = make_classification(n_samples=10000, n_classes=3, n_redundant=0, n_repeated=0,
                           n_features=2, class_sep=10, n_clusters_per_class=1, n_informative=2, random_state=76)


# sns.scatterplot(X[0:, 0], X[0:, 1], hue=y[0:]+1, s=50,
#                 palette=["red", "green", "blue"])
# plt.title(f"Random State {76}")
# plt.show()


row_length = X.shape[0]
sample_0 = X[y == 0].tolist()
sample_1 = X[y == 1].tolist()
sample_2 = X[y == 2].tolist()

X = sample_0[: 10]
y = [0] * 10
X.extend(sample_1[:10])
y.extend([1] * 10)
X.extend(sample_2[:10])
y.extend([2] * 10)
X = np.array(X)
y = np.array(y)

# The classification SVC model
model = SVC(kernel="linear")
clf = model.fit(X, y)

fig, ax = plt.subplots()

# Set-up grid for plotting.
X0, X1 = X[:, 0], X[:, 1]
xx, yy = make_meshgrid(X0, X1)
title = "Linear Decision Boundary"
plot_contours(ax, clf, xx, yy, cmap=plt.cm.coolwarm, alpha=0.8)
ax.scatter(X0, X1, c=y, cmap=plt.cm.coolwarm, s=20, edgecolors="k")
ax.set_ylabel("{}".format("Feature 1"))
ax.set_xlabel("{}".format("Feature 2"))
ax.set_xticks(())
ax.set_yticks(())
ax.set_title(title)
# plt.show()

plotting_support_vector(X, y)

"""
new_sample = []

for i in range(10):
    (new_x, new_y) = ((sample_0[i][0] + sample_1[i][0] + sample_2[i]
                       [0]) / 3, (sample_0[i][1] + sample_1[i][1] + sample_2[i][1]) / 3)
    print([new_x, new_y])
"""

"""
X = sample_0[: 10]
y = [0] * 10
X.extend(sample_1[:10])
y.extend([1] * 10)
X.extend(sample_2[:10])
y.extend([2] * 10)
X = np.array(X)
y = np.array(y)

sns.scatterplot(X[0:, 0], X[0:, 1], hue=y+1, s=50,
                palette=["red", "green", "blue"])
plt.title("Linearly Separable Dataset")
# plt.savefig("./dataset.png")
plt.show()
"""


# print("------------------- Class 2 ------------")
# print(sample_1[: 10])
# print("------------- Class 3 -------------")
# print(sample_2[: 10])

# sns.scatterplot(X[0:, 0], X[0:, 1], hue=y+1, s=50,
#                 palette=["red", "green", "blue"])
# plt.title("Linearly Separable Dataset")
# plt.savefig("./dataset.png")
# plt.show()
