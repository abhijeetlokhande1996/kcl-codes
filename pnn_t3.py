import numpy as np
import pandas as pd
def q2():
    print("Q2 Output: ")
    w = np.array([0.1, -0.5, 0.4])
    x1 = np.array([0.1, -0.5, 0.4])
    x2 = np.array([0.1,0.5,0.4])

    y1 = 1 if np.sum(w * x1) > 0 else 0
    y2 = 1 if np.sum(w * x2) > 0 else 0
    print(f"Y1 {y1} Y2 {y2}")

def q3():
    print("Q3 Output: ")
    test = np.array([0, 1])
    y = np.array([1, 0])
    theta = 1.5
    w1 = 2
    eta = 1
    w = np.append(np.array(-theta), np.array([w1]))
    epoch = 0
    while True:
        pred_list = []
        for idx, x in enumerate(test):
            t = y[idx]
            x = np.append(np.array(1), x)
            h = 1 if np.sum(w * x) > 0 else 0
            w = w + eta * (t - h) * x
            #print(w)
            pred_list.append(h)
        epoch += 1
        if pred_list == list(y):
            print("Algorithm converge at ", epoch)
            print(w)
            break

def q4():
    print("Q4 Output: ")
    test = np.array([0, 1])
    y = np.array([1, 0])
    theta = 1.5
    w1 = 2
    eta = 1
    epoch = 30
    w = np.append(np.array([-theta]), np.array([w1]))
    for _ in range(epoch):
        tmp = [0, 0]
        pred = []
        for idx, x in enumerate(test):
            t = y[idx]
            x = np.append(np.array([1]), x)
            h = 1 if np.sum(w * x) > 0 else 0
            pred.append(h)
            tmp[0] += (eta * (t - h) * x)[0] 
            tmp[1] += (eta * (t - h) * x) [1]
        w = w + tmp
        if pred == list(y):
            print("Final Weights: ", w, " Converged at ", _+1, " Epoch")
            break

def q5():
    print("Q5 Output: ")
    test = [[0,0], [0,1], [1,0],[1,1]]
    y = np.array([0,0,0,1])
    eta = 1
    w1 = 1
    w2 = 1
    theta = -0.5
    w = np.array([-theta, w1, w2])
    epoch = 700
    for e in range(epoch):
        pred = []
        for idx, x in enumerate(test):
            t = y[idx]
            x = np.append(np.array([1]), np.array(x))
            h = 1 if np.sum(w * x) > 0 else 0
            w = w + eta * (t - h) * x
            # print(w)
            pred.append(h)
        if pred == list(y):
            print("Final weights: ", w)
            break
def q6():

    print("Q6 Output: ")
    test = [[0,2], [1,2], [2,1],[-3,1], [-2, -1], [-3, -2]]
    y = np.array([1,1,1,0,0,0])
    eta = 1
    w1 = 0
    w2 = 0
    theta = -1
    w = np.array([-theta, w1, w2])
    epoch = 700
    for e in range(epoch):
        pred = []
        for idx, x in enumerate(test):
            t = y[idx]
            x = np.append(np.array([1]), np.array(x))
            h = 1 if np.sum(w * x) > 0 else 0
            w = w + eta * (t - h) * x
            # print(w)
            pred.append(h)
        if pred == list(y):
            print("Final weights: ", w)
            break

def cwq1():
    print("CWQ1 Output: ")
    test = [[0,2], [1,2], [2,1],[-3,1], [-2, -1], [-3, -2]]
    y = np.array([1,1,1,0,0,0])
    eta = 1
    w1 = -5.5
    w2 = -2.5
    theta = -6.5
    w = np.array([-theta, w1, w2])

    epoch = 2
    _iter = 0

    for e in range(epoch):
        pred = []
        for idx, x in enumerate(test):
            t = y[idx]
            x = np.append(np.array([1]), np.array(x))
            # h = 1 if np.sum(w * x) > 0 else 0
            h = np.sum(w * x)
            if h == 0:
                h = 0.5
            elif h > 0:
                h = 1
            else:
                h = 0
            w = w + eta * (t - h) * x
            print(f"Iteration A  {_iter + 1}: {h} {w}")
            _iter += 1
            # print(w)
            pred.append(h)
    print("Final Weights: ", w)
    # print(y)
    # print(pred)

def cwq2():
    print("CWQ2 Output: ")
    import numpy as np
    import pandas as pd
    from sklearn.datasets import load_iris
    from sklearn.metrics import accuracy_score
    test = load_iris()["data"]
    y  = load_iris()["target"]
    # print(target)
    for idx, el in enumerate(y):
        y[idx] = 1 if el == 0 else 0
#    print(y)
    
    eta = 0.10
    w1 = -2.5
    w2 = 3.5
    w3 = 1.5
    w4 = 0.5
    theta = 0.5
    
    w = np.array([-theta, w1, w2, w3, w4])
    epoch = 2
    _iter = 0
    print("---------- Before Learning ----------")
    y_pred = []
    for x in test:
        x = np.append(np.array([1]), np.array(x))
        p = np.sum(w * x)
        if p == 0:
            h = 0.5
        elif p > 0:
            h = 1
        else:
            h = 0
        y_pred.append(h)

#    print("Accuracy Score Before Learning: ", accuracy_score(list(y), y_pred))
    c_count = 0
    for idx, pred in enumerate(y_pred):
        if pred == y[idx]:
            c_count += 1
    # print(y_pred)    
    print("Accuracy Score Before Learning: (Without using sklearn): ", (c_count / y.shape[0]) )

    print("----------- After Learning ----------")
    for e in range(epoch):
        pred = []
        for idx, x in enumerate(test):
            t = y[idx]
            x = np.append(np.array([1]), np.array(x))
            h = np.sum(w * x)
            if h == 0:
                h = 0.5
            elif h > 0:
                h = 1
            else:
                h = 0
            w = w + eta * (t - h) * x
            # print(f"Iteration {_iter + 1} {w}")
            _iter += 1
            # print(w)
            pred.append(h)
    print(f"Final Weights: After {epoch}", w)
    y_pred = []
    for x in test:
        x = np.append(np.array([1]), np.array(x))
        p = np.sum(w * x)
        if p == 0:
            h = 0.5
        elif p > 0:
            h = 1
        else:
            h = 0
        # h = 1 if np.sum(w * x) > 0 else 0
        y_pred.append(h)
    c_count = 0
    for idx, el in enumerate(y_pred):
        if el == y[idx]:
            c_count += 1

    print("Accuracy after learning is: ", c_count / y.shape[0])
    #print("Accuracy Score After Training: ", accuracy_score(list(y), y_pred))
if __name__ == "__main__":
    q2()
    q3()
    q4()
    q5()
    q6()
    cwq1()
    cwq2()
