import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.preprocessing import MinMaxScaler
from matplotlib import pyplot as plt
import math

def calculate_distance(pl1, pl2):
    distance = 0
    for idx in range(len(pl1)):
        p1 = pl1[idx]
        p2 = pl2[idx]
        distance += (p1 - p2) ** 2
    return math.sqrt(distance)

def within_cluster_distance(center, data):
    center = list(center)
    distance = 0
    for instance in data:
        distance += calculate_distance(center, instance)
    
    # print("Within cluster Distance ", distance)
    return distance
df = pd.read_csv("./data/wholesale_customers.csv")
df = df.drop(["Channel", "Region"], axis=1)
"""
mean_of_instance = {}
for idx, row in df.iterrows():
    mean_of_instance[idx] = row.mean()

mean_df  = pd.Series(mean_of_instance).to_frame()
mean_df.columns = ["Mean"]

min_max_of_col = []
for col in df.columns:
    min_max_of_col.append([df[col].min(), df[col].max()])

min_max_df = pd.DataFrame(data=min_max_of_col, columns = ["Min", "Max"], index = df.columns)
"""
x = df.values
kmeans = KMeans(n_clusters=3, init="k-means++", max_iter=500, n_init=10)
y_kmeans = kmeans.fit_predict(x)

clusters = np.unique(kmeans)


fig, axs = plt.subplots(5, 1)
fig.tight_layout()

for idx, col in enumerate(df.columns):
    if idx == len(df.columns) - 1:
        continue
    axs[idx].scatter(x[y_kmeans == 0, idx], x[ y_kmeans == 0, idx+1], c="red", marker="s", edgecolor="black", label="Cluster 1")
    axs[idx].scatter(x[y_kmeans == 1, idx], x[ y_kmeans == 1, idx+1], c="green", marker="s", edgecolor="black", label="Cluster 2")
    axs[idx].scatter(x[y_kmeans == 2, idx], x[ y_kmeans == 2, idx+1], c="blue", marker="s", edgecolor="black", label="Cluster 3")
    axs[idx].scatter(kmeans.cluster_centers_[:, idx], kmeans.cluster_centers_[:, idx+1], marker="*" ,s=300, c='yellow', label = 'Centroids')
    axs[idx].set_xlabel(col)
    axs[idx].set_ylabel(df.columns[idx + 1])
    print(col, df.columns[idx + 1])
"""
    axs[idx][0].scatter(x[kmeans == 0, idx], x[kmeans==0, idx+1], c="green", marker="s", edgecolors="black", label="Cluster 1",s=50)
    axs[idx][0].set_title("Cluster 1")
    axs[idx][0].set_xlabel(col)
    
    axs[idx][1].scatter(x[kmeans == 1, idx], x[kmeans==1, idx+1], c="orange", marker="s", edgecolors="black", label="Cluster 2",s=50)
    axs[idx][1].set_xlabel(col)
    axs[idx][1].set_ylabel(df.columns[idx + 1])
    axs[idx][1].set_title("Cluster 2")
    
    axs[idx][2].scatter(x[kmeans == 2, idx], x[kmeans==2, idx+1], c="blue", marker="s", edgecolors="black", label="Cluster 3",s=50)
    axs[idx][2].set_xlabel(col)
    axs[idx][2].set_ylabel(df.columns[idx + 1])
    axs[idx][2].set_title("Cluster 3")
"""
# plt.show()


# X = MinMaxScaler().fit_transform(df.values)
X = df.values
bc_distance_dict = {}
wc_distance_dict = {}
for k in [3, 5, 10]:
    if not k in bc_distance_dict:
        bc_distance_dict[k] = 0

    if not k in wc_distance_dict:
        wc_distance_dict[k] = 0
    kmeans = KMeans(n_clusters=k).fit(X)
#    print(f"K = {k}", kmeans.inertia_)
    wc_distance_dict[k] = kmeans.inertia_
    # pred = kmeans.predict(X)
    
    bc_distance = 0
    for idx in range(kmeans.cluster_centers_.shape[0]):
        for idy in range(idx + 1, kmeans.cluster_centers_.shape[0]):
            bc_distance += calculate_distance(kmeans.cluster_centers_[idx], kmeans.cluster_centers_[idy])
    bc_distance_dict[k] = bc_distance
    # print( kmeans.cluster_centers_[0].shape, df.values[0].shape)
    
    # within cluster distance calculation
    """
    cluster_data_mapping = {}
    for idx, row in df.iterrows():
        cluster_name = pred[idx]
        if not cluster_name in cluster_data_mapping:
            cluster_data_mapping[cluster_name] = []
        cluster_data_mapping[cluster_name].append(list(row))

    distance = 0    
    for idx, key in enumerate(list(cluster_data_mapping.keys())):
        data = cluster_data_mapping[key]
        # print(kmeans.cluster_centers_[idx])
        d = within_cluster_distance(kmeans.cluster_centers_[idx], data)
        distance += d
    wc_distance_dict[k] = distance
    """
    # print(f"within cluster distance for k = {k} distance is {distance}")

"""
print("BC Distance ")
print(bc_distance_dict)
print("WC Distance")
print(wc_distance_dict)

"""

bcs = pd.Series(bc_distance_dict)
wcs = pd.Series(wc_distance_dict)
ratio = {}
for k in bc_distance_dict.keys():
    ratio[k] = bc_distance_dict[k] / wc_distance_dict[k]

ratio = pd.Series(ratio)
info = pd.DataFrame([wcs, bcs, ratio], index=["BC", "WC", "BC/WC"])
print(info)
