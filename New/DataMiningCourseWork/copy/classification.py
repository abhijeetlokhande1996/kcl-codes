import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder, OrdinalEncoder
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import KFold, train_test_split



adult = pd.read_csv("./../data/adult.csv", encoding="utf-8")
adult = adult.fillna(np.nan)

adult = adult.drop(["fnlwgt"], axis=1)

for idx, el in adult["native-country"].items():
    if str(el) == "nan":
        adult.loc[idx, "native-country"] = np.nan

print("Number of instance: ", len(adult.index))

_sum = 0
for col in adult.columns:
    _sum += adult.isna().sum()[col]
print("Number of missing values: ", _sum)
print("Fraction of missing values over all attribute values: ", _sum / adult.size)


_count = 0
for idx, el in adult.isna().sum(axis=1).items():
    if el > 0:
        _count += 1
print("Number of instance with missing values: ", _count)
print("Fraction of instances with missing values over all instances: ", _count / adult.shape[0])

# print(cat_col)


_adult = adult.copy()
# Label Encoder
# Ignoring the nan values and rest of the values are encoding

_adult = _adult.apply(lambda s: pd.Series( LabelEncoder().fit_transform(s[s.notnull()]), index=s[s.notnull()].index ))

for col in _adult.columns:
    print(f"Discrete values of {col}: ", col)
    print(np.unique(_adult[col][~np.isnan(_adult[col])]))
    # print(_adult[col].unique())


# dropping nan values for training
_adult = _adult.dropna()
y = _adult["class"]
_adult = _adult.drop(["class"], axis=1)

"""
# _adult dataframe with no missing values in the instance
# data in dataset is not large. Kfold cross validation is good choice to apply rather train_test_split
"""
max_score = 0
f_clf = None
kf = KFold(n_splits=10, shuffle=True)
clf = DecisionTreeClassifier()

for i in range(10):
    train_test_index = next(kf.split(_adult))
    X_train = _adult.iloc[train_test_index[0]]
    y_train = y.iloc[train_test_index[0]]
    X_test = _adult.iloc[train_test_index[1]]
    y_test = y.iloc[train_test_index[1]]
    clf.fit(X_train, y_train)
    score = clf.score(X_test, y_test)
    if i == 0:
        max_score = score
        f_clf = clf
    else:
        if max_score < score:
            max_score = score
            f_clf = clf

print("Maximum score on decision tree model on KFold cross validation is: ", max_score)




# Constructing D' from containing all instances it atleast one missing value and equal number of instances without missing values
"""
rows_missing_val = []
for idx, row in adult.iterrows():
    if row.isna().sum() > 0:
        rows_missing_val.append(row)
"""

# adult dataset is no LabelEncoded
adult_missing_val =  adult[adult.isna().any(axis='columns')]
adult_no_missing_val = adult.dropna().sample(len(adult_missing_val.index))
d_prime = adult_missing_val.append(adult_no_missing_val)
d1_prime = d_prime.fillna("missing")

d2_prime = d_prime.copy()
# replacing missing values with mode
for col in d2_prime:
    if d2_prime[col].isna().sum() > 0:
        _mode = d2_prime[col].mode()[0]     
        d2_prime[col] = d2_prime[col].fillna(_mode)

# encode data in d1_prime (ordinal encoder is same as label encoded)
target_d1_prime = d1_prime["class"]
d1_prime = d1_prime.drop(["class"], axis=1)

ord_encoder = OrdinalEncoder()
encoded_data = ord_encoder.fit_transform(d1_prime[d1_prime.columns])

X_train, X_test, y_train, y_test = train_test_split(encoded_data, target_d1_prime, test_size=0.2)
clf_d1_prime = DecisionTreeClassifier()
clf_d1_prime.fit(X_train, y_train)
print("Accuracy Decision Tree on d1_prime test data: ", clf_d1_prime.score(X_test, y_test))

# Decision Tree making on d2_prime
target_d2_prime = d2_prime["class"]
d2_prime = d2_prime.drop(["class"], axis=1)


ord_encoder = OrdinalEncoder()
encoded_data = ord_encoder.fit_transform(d2_prime[d2_prime.columns])

X_train, X_test, y_train, y_test = train_test_split(encoded_data, target_d2_prime, test_size=0.2)

clf_d2_prime = DecisionTreeClassifier()
clf_d2_prime.fit(X_train, y_train)

print("Accuracy Decision Tree on d2_prime test data: ", clf_d2_prime.score(X_test, y_test))

# targe variable in _adult is 0 or 1
# but we need <50 and >50 target variable because d1_prime and d2_prime models uses on <50 and >50 class label
# getting samples from _adult
# when we performed dropna operation on _adult  we did not perform reset index 
# therefore adult df index is super set of _adult index
# we are getting actual class label from adult df and we picking it up by index of data which we already sliced from _adult

test_data = _adult.sample(100)
test_result = adult["class"][test_data.index]

score_d1_prime = clf_d1_prime.score(test_data, test_result)
score_d2_prime = clf_d2_prime.score(test_data, test_result)
print("Score of  d1_prime model and d2_prime model on original dataset", score_d1_prime, score_d2_prime)
