from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score,accuracy_score, recall_score, f1_score

iris = load_iris()
data = iris["data"]
target = iris["target"]
print(iris["target"])
x_train, x_test, y_train, y_test = train_test_split(data, target, test_size=0.2, random_state=0)
clf = DecisionTreeClassifier().fit(x_train, y_train)
print("Score: ", clf.score(x_test, y_test))

print("Accuracy: ", accuracy_score(clf.predict(x_test), y_test))
print("Confusion Matrix: ", confusion_matrix(clf.predict(x_test), y_test))
"""
    for precion score target should be binary
"""
y_hat = clf.predict(x_test)
# print("Pred: ", y_hat)
print("Precision Score: ", precision_score(y_hat, y_test, average= None))
print("Recall Score: ", recall_score(y_hat, y_test, average=None))
print("F1 Score: ", f1_score(y_hat, y_test, average=None))

decision_path = clf.decision_path(data)
# print(decision_path)

# Visualise decision tree with graphviz
export_graphviz(clf, out_file="iris-tree.dot", class_names=iris["target_names"], impurity=True)

"""
	Logistic Regression
"""
from sklearn.linear_model import LogisticRegression
clf = LogisticRegression(max_iter=1000).fit(x_train, y_train)
print("Logistic Regression  Score: ", clf.score(x_test, y_test))
y_hat = clf.predict(x_test)
print("Confusion Matrix Logistic Regression: \n", confusion_matrix(y_hat, y_test))
