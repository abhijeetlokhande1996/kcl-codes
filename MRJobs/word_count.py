from functools import reduce
from mrjob.job import MRJob
from mrjob.step import MRStep


class WordCount(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper, reducer=self.reducer),
            # MRStep(reducer=self.reducer2)
        ]

    def mapper(self, key, value):
        words_arr = value.split(",")
        if len(words_arr) > 1:
            yield "Age", float(words_arr[0])

    def reducer(self, key, value):
        age_list = list(value)
        _sum = sum(age_list)
        _average = _sum / len(age_list)
        yield f"{key}", {"Sum": _sum, "Average": _average}

    # def reducer2(self, ket, value):
    #     yield "AverageAge", list(value)


if __name__ == "__main__":
    WordCount.run()
