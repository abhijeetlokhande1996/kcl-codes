from typing import ValuesView
from mrjob.job import MRJob


class AveragePerWorkClass(MRJob):
    def mapper(self, key, value):
        words_arr = value.split(",")
        if len(words_arr) > 2:
            if words_arr[1].strip() != "?":
                yield words_arr[1].strip(), float(words_arr[0])

    def reducer(self, key, age):
        age_list = list(age)
        _sum = sum(age_list)
        average = sum(age_list) / len(age_list)
        yield key, {"Sum": _sum, "Average": average}


if __name__ == "__main__":
    AveragePerWorkClass.run()
