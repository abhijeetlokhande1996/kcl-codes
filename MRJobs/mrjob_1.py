from mrjob.job import MRJob


class MRJWord(MRJob):
    def mapper(self, _, line):
        words = line.split()
        for word in words:
            yield word, 1

    def reducer(self, key, value):
        yield key, sum(value)


MRJWord.run()
