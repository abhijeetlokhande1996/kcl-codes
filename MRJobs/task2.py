from mrjob.job import MRJob
from mrjob.step import MRStep


class TenMostFrequentAgeValues(MRJob):
    # defining mapper and reducer
    # we are using 2 reducer
    # "reducer" for yield "Age Frequency Mapping"
    # "reducer_2" for yield sorted top 10 AgeFreqMapping
    def steps(self):
        return [
            MRStep(mapper=self.mapper, reducer=self.reducer),
            MRStep(reducer=self.reducer_2)
        ]

    def mapper(self, key, lines):
        # splitting line into words
        words_arr = lines.split(",")
        # safe checking length of words_arr
        if len(words_arr) > 2:
            age = float(words_arr[0])
            yield age, 1

    def reducer(self, age, value):
        # calculating frequency of each age
        yield "AgeFreqMapping", {"Age": age, "Freq": sum(value)}

    def reducer_2(self, age, age_freq_mapping):
        # making array from dictionary
        tmp = []
        for item in age_freq_mapping:
            item = dict(item)
            tmp.append((item["Age"], item["Freq"]))
        # sorting dict
        # yeilding only top 10 highest Frequencies
        for age, freq in sorted(tmp, key=lambda item: item[1], reverse=True):
            yield freq, f"{age}"


if __name__ == "__main__":
    # execute MRJob
    TenMostFrequentAgeValues.run()
