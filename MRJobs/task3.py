from mrjob.job import MRJob
from mrjob.step import MRStep


class InvertedIndex(MRJob):
    # defining steps
    # I am using one mapper and 2 reducer
    def steps(self):
        return [
            MRStep(mapper=self.mapper, reducer=self.reducer_1),
            MRStep(reducer=self.reducer_2)
        ]

    # mapper to
    # split lines into the words
    # and making of array of line_id and symbol/value
    # line id present in index 0
    # symbols are present from index 1
    # yielding a list which contain line_id and symbols
    def mapper(self, key, line):
        words_arr = [word.strip() for word in line.split()]
        line_id = words_arr[0]
        symbols = words_arr[1:]
        yield None, [line_id, symbols]

    # making a dictionary
    # dictionary is containing symbol as a key
    # and line_id as a value in the associated array
    def reducer_1(self, key, value):

        symbol_line_id_mapping = {}
        for item in list(value):
            line_id = int(item[0])
            symbols = item[1]
            for s in symbols:
                if not s in symbol_line_id_mapping:
                    symbol_line_id_mapping[s] = []
                symbol_line_id_mapping[s].append(line_id)
        yield "symbol_line_id_mapping", symbol_line_id_mapping

    # reducer_2 is only for sorting values associated key
    # we can write reducer_2 code in reducer_1 as well, no need of reducer_2
    # but I want to write more cleaner code therefore I made reducer_2
    def reducer_2(self, key, symbol_line_id_mapping):
        for item in symbol_line_id_mapping:
            for symbol in item.keys():
                # removing duplicate values and sorting in ascending order
                yield symbol, sorted(list(set(item[symbol])))


if __name__ == "__main__":
    # it execute MRJob
    InvertedIndex.run()
