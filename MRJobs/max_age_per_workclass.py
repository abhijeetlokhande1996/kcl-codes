from mrjob.job import MRJob


class MaxAgePerWorkClass(MRJob):
    def mapper(self, key, value):
        words_arr = value.split(",")
        if len(words_arr) > 2:
            age = float(words_arr[0])
            workclass = words_arr[1].strip()
            yield workclass, age

    def reducer(self, key, value):
        # get max age per class
        yield key, max(value)


if __name__ == "__main__":
    MaxAgePerWorkClass.run()
