
from mrjob.job import MRJob
from mrjob.step import MRStep
import sys


class MRInvertedIndex(MRJob):

    def mapper(self, _, line):
        data = line.split()
        index = data[0]
        values = data[1:]
        yield None, [index, values]

    def reducer(self, key, value):

        index_dict = {}
        for eachValue in value:
            line_id = eachValue[0]
            symbols = eachValue[1]
            for eachSymbol in symbols:
                if not eachSymbol in index_dict:
                    index_dict[eachSymbol] = []
                # if not line_id in index_dict[eachSymbol]:
                index_dict[eachSymbol].append(line_id)
        count = 0
        for itr in index_dict:
            count += 1
            if count < 11:
                yield itr, list(set(index_dict[itr]))


if __name__ == '__main__':
    MRInvertedIndex.run()
