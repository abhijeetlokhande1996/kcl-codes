# mlLearningAgents.py
# parsons/27-mar-2017
#
# A stub for a reinforcement learning agent to work with the Pacman
# piece of the Berkeley AI project:
#
# http://ai.berkeley.edu/reinforcement.html
#
# As required by the licensing agreement for the PacMan AI we have:
#
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).

# The agent here was written by Simon Parsons, based on the code in
# pacmanAgents.py
# learningAgents.py

from numpy.testing._private.utils import rand
from pacman import Directions
from game import Actions, Agent
import random
import game
import util
import numpy as np
import sys
# QLearnAgent
#


class QLearnAgent(Agent):

    # Constructor, called when we start running the
    def __init__(self, alpha=0.2, epsilon=0.05, gamma=0.8, numTraining=10):
        # alpha       - learning rate
        # epsilon     - exploration rate
        # gamma       - discount factor
        # numTraining - number of training episodes
        #
        # These values are either passed from the command line or are
        # set to the default values above. We need to create and set
        # variables for them
        self.alpha = float(alpha)
        self.epsilon = float(epsilon)
        self.gamma = float(gamma)
        self.numTraining = int(numTraining)
        # Count the number of games we have played
        self.episodesSoFar = 0

        # dictionary to store Q value map
        # key pacman coordinates and value should be array of q-values

        self.q_values = {}
        self.actions = ["North", "South", "East", "West"]
        # variable to store last pacman state or pos
        self.last_pacman_state = None
        # variable to store last action taken
        self.last_action = None
        np.random.seed(42)
    # Accessor functions for the variable episodesSoFars controlling learning

    def incrementEpisodesSoFar(self):
        self.episodesSoFar += 1

    def getEpisodesSoFar(self):
        return self.episodesSoFar

    def getNumTraining(self):
        return self.numTraining

    # Accessor functions for parameters
    def setEpsilon(self, value):
        self.epsilon = value

    def getAlpha(self):
        return self.alpha

    def setAlpha(self, value):
        self.alpha = value

    def getGamma(self):
        return self.gamma

    def getMaxAttempts(self):
        return self.maxAttempts

    # method to return next action

    def get_next_action(self, legal, current_pos):
        # exploitation
        if np.random.random() > self.epsilon:
            q_values_legal = []
            # iterating over legal actions of current pacman position
            # storing q_values of legal actions to one array "q_values_legal"
            # desired action will be the action which has maximum q_value
            for legal_dir in legal:
                action_index = self.actions.index(legal_dir)
                q_values_legal.append(self.q_values[current_pos][action_index])
            desired_action = legal[np.argmax(q_values_legal)]
            return desired_action
        else:
            # exploration
            # return random legal action
            return np.random.choice(legal)
    # getAction
    #
    # The main method required by the game. Called every time that
    # Pacman is expected to move

    def getAction(self, state):
        # checking whether the current state has entry in q_value table
        # if not create it and initialise with zeros
        if state not in self.q_values:
            self.q_values[state] = [0.0, 0.0, 0.0, 0.0]

        legal = state.getLegalPacmanActions()
        if Directions.STOP in legal:
            legal.remove(Directions.STOP)
        action_taken = None
        # this if statement executes when pacman moves very first time
        # at that time just return random legal action
        # and set last_pacman_state variable
        if not self.last_pacman_state:

            action = np.random.choice(legal)

            action_taken = action
        else:
            # this if loop executes when last pacman position or state is available
            # generating next best action
            action = self.get_next_action(
                legal=legal, current_pos=state)

            # calculate immediate reward of current state
            ir = state.getScore() - self.last_pacman_state.getScore()

            # extraction prevous state and previous action q value from q_values dict
            old_q_value = self.q_values[self.last_pacman_state][self.actions.index(
                self.last_action)]

            # calculating state value of current state
            current_state_value = np.max(self.q_values[state])
            td_second_term = old_q_value
            # calculating temporal difference
            # gamma is discount factor
            td = ir + (self.gamma * current_state_value) - td_second_term
            # q value update rule
            # alpha is learning rate
            new_q_value = old_q_value + (self.alpha * td)

            # updating old q value of previous state and previous action with new one
            self.q_values[self.last_pacman_state][self.actions.index(
                self.last_action)] = new_q_value

            action_taken = action
        # updating last pacman state
        self.last_pacman_state = state
        # updating last pacman position
        self.last_action = action_taken
        return action_taken

    def final(self, state):
        # this method executes when pacman die
        # calculating immediate reward
        ir = state.getScore() - self.last_pacman_state.getScore()

        # extracting q value of previous state and previous action
        old_q_value = self.q_values[self.last_pacman_state][self.actions.index(
            self.last_action)]

        # current_state_value is zero because at there is not state value of terminating state
        # as we don't have any action to perform on terminating state.
        current_state_value = 0.0
        td_second_term = old_q_value
        # calculating temporal diifference
        td = ir + (self.gamma * current_state_value) - td_second_term
        # update rule
        new_q_value = old_q_value + (self.alpha * td)
        # update old q value with new value in q_values dict
        self.q_values[self.last_pacman_state][self.actions.index(
            self.last_action)] = new_q_value
        # resetting the variable
        self.last_pacman_state = None
        self.last_action = None
        # we need to reduce exploration rate as we are learning
        ep = 1 - self.getEpisodesSoFar()*1.0/self.getNumTraining()
        # setting the new epsilon, which the less than the previous epsilon
        self.setEpsilon(ep*0.1)
        print "A game just ended!"

        # Keep track of the number of games played, and set learning
        # parameters to zero when we are done with the pre-set number
        # of training episodes
        self.incrementEpisodesSoFar()
        if self.getEpisodesSoFar() == self.getNumTraining():
            msg = 'Training Done (turning off epsilon and alpha)'
            print '%s\n%s' % (msg, '-' * len(msg))
            self.setAlpha(0)
            self.setEpsilon(0)


# python 2.7 script
# Chen/13-apr-2017
# based on the script written by Parsons
#
#
# A stub for a reinforcement learning agent to work with the Pacman
# piece of the Berkeley AI project:
#
# http://ai.berkeley.edu/reinforcement.html
#
# As required by the licensing agreement for the PacMan AI we have:
#
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).
