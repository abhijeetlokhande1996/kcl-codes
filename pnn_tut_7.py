import numpy as np


def Q4():
    print("Q4 Output: ")
    X = np.array([[1, 2, 1], [2, 3, 1], [3, 5, 1], [2, 2, 1]])
    N = X.shape[0]
    mean = np.mean(X, axis=0)
    X_prime = (X - mean).T
    C = X_prime @ X_prime.T / N
    w, v = np.linalg.eigh(C)
    v_hat = []
    for idx in np.argsort(w)[::-1]:
        if w[idx] == 0:
            continue
        v_hat.append(v[:, idx])
    v_hat = np.array(v_hat)
    ans = (v_hat @ X_prime).T
    print(ans)


def Q6():
    print("Q5 Output: ")
    X = np.array([[0, 1], [3, 5], [5, 4], [5, 6], [8, 7], [9, 7]])
    N = X.shape[0]
    mean = np.mean(X, axis=0)
    X_prime = (X - mean).T
    C = X_prime @ X_prime.T / N
    w, v = np.linalg.eigh(C)
    w = np.diag(w)
    print(w)
    print(v)
    # difference between eighen values are large
    # so selecting only one largest value and corresponding eighen vector
    v = v[:, -1].reshape(-1, 1)
    ans = v.T @ X_prime
    print(ans.reshape(-1))


def Q7():
    """
        y = w*x
        w = w + eta * y(x.T - y*w)
        y(x.T - y*w) is weight change, we have to calculate total weight change then update a weight
    """
    print("Q7 Output: ")
    w = np.array([-1, 0]).reshape(-1, 1)
    eta = 0.01
    X = np.array([[-5, -4], [-2, 0], [0, -1], [0, 1], [3, 2], [4, 2]])
    for i in range(6):
        total_weight_change = 0
        for ip in X:
            ip = ip.reshape(-1, 1)
            y = (w.T @ ip).reshape(-1)[0]
            total_weight_change += eta * y * (ip - w * y)
        w = w + total_weight_change
        ans = np.round(w.reshape(-1), 3)
        print(f"Weight at epoch {i + 1} {ans}")


def Q10():
    print("Q10 Output: ")
    W = np.array([[-1, 5], [2, -3]])
    X = np.array([[1, 2], [2, 1], [3, 3], [6, 5], [7, 8]])
    y = np.array([1, 1, 1, 2, 2])
    mean_1 = np.mean(X[y == 1], axis=0)
    mean_2 = np.mean(X[y == 2], axis=0)
    for w in W:
        sb = (w @ (mean_1 - mean_2)) ** 2
        sw = 0
        for x in X[y == 1]:
            sw += (w @ (x - mean_1)) ** 2
        for x in X[y == 2]:
            sw += (w @ (x - mean_2)) ** 2
        print("Cost: ", sb/sw)
    # Cost of weight is [-1, 5] is higher 
    # meaning [-1, 5] it is more effective if we project the data
    # using this weight our data will be linearly separable

    # for w in W:
    #     print(w)


if __name__ == "__main__":
    # Q4()
    Q6()
    # Q7()
    # Q10()
