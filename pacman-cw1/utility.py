# Author: Abhijeet Santosh Lokhande
# File contains some utility functions and class
import numpy as np
from scipy.special import expit

class NaiveBayes:
    def fit(self, X, y):
        n_sample, n_features = X.shape
        self._classes = np.unique(y)
        n_classes = len(self._classes)
        # init mean, variance and priors

        # mean of each input feature associated with each class_labe
        self._mean = np.zeros((n_classes, n_features), dtype=np.float64)
        self._var = np.zeros((n_classes, n_features), dtype=np.float64)
        self._priors = np.zeros(n_classes, dtype=np.float64)
        for idx, c in enumerate(self._classes):
            X_c = X[y==c]
            # column wise mean
            self._mean[idx,: ] = X_c.mean(axis=0)
            self._var[idx,: ] = X_c.var(axis=0)
            # safe side converting deno to float
            self._priors[idx] = X_c.shape[0] / float(n_sample)
     #   print(self._mean[0: 10])
    def predict(self, X):
        y_pred = [self._predict(x) for x in X]
        return np.array(y_pred)

    def _predict(self, x):
        posteriors_list = []
        for idx, c in enumerate(self._classes):
            # taking a log because of number exploding
            prior = np.log(self._priors[idx])
            posterior = np.sum(np.log(self._pdf(idx, x)))
            posterior = prior + posterior
            posteriors_list.append(posterior)
        return self._classes[np.argmax(posteriors_list)]

    def _pdf(self, class_idx, x):
        mean = self._mean[class_idx]
        var = self._var[class_idx]
        numeratore = np.exp(-(x - mean)**2 / (2 * var ) )
        denominator = np.sqrt(2 * np.pi * var)
        return numeratore / denominator













class NeuralNetwork(object):
    def __init__(self, inputnodes, hiddennodes, outputnodes, learningrate):
        self.inodes = inputnodes
        self.hnodes = hiddennodes
        self.onodes = outputnodes
        self.lr = learningrate
        self.wih = np.random.normal(0, pow(2 / self.hnodes + self.inodes, -0.5), (self.hnodes, self.inodes))
        self.who = np.random.normal(0, pow(2 / self.onodes + self.hnodes , -0.5), (self.onodes, self.hnodes))
        self.activation_function = lambda x: expit(x)
    def train(self, inputs_list, targets_list):
        inputs = np.array(inputs_list, ndmin=2).T
        targets = np.array(targets_list, ndmin=2).T
        hidden_inputs = np.dot(self.wih, inputs)
        hidden_outputs = self.activation_function(hidden_inputs)
        final_inputs = np.dot(self.who, hidden_outputs)
        final_outputs = self.activation_function(final_inputs)
        output_errors = targets - final_outputs
        hidden_errors = np.dot(self.who.T, output_errors)
        self.who += self.lr * np.dot((output_errors * final_outputs * (1 - final_outputs)), hidden_outputs.T)
        self.wih += self.lr * np.dot((hidden_errors * hidden_outputs * (1 - hidden_outputs)), inputs.T)
    def query(self, input_list):
        # convert input list to 2d array
        inputs = np.array(input_list, ndmin=2).T
        hidden_inputs = np.dot(self.wih, inputs)
        hidden_outputs = self.activation_function(hidden_inputs)
        final_inputs = np.dot(self.who, hidden_outputs)
        final_outputs = self.activation_function(final_inputs)
        return final_outputs

def accuracy(y_true, y_pred):
    accuracy = np.sum(y_true == y_pred) / len(y_true)
    return accuracy




def filter_array_from_index(source, index_list):
    result = []
    for idx in index_list:
        result.append(source[idx])
    return result

def scale_data(data):
    return data
