# classifierAgents.py
# parsons/07-oct-2017
#
# Version 1.0
#
# Some simple agents to work with the PacMan AI projects from:
#
# http://ai.berkeley.edu/
#
# These use a simple API that allow us to control Pacman's interaction with
# the environment adding a layer on top of the AI Berkeley code.
#
# As required by the licensing agreement for the PacMan AI we have:
#
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).

# The agents here are extensions written by Simon Parsons, based on the code in
# pacmanAgents.py

from io import SEEK_CUR
from pacman import Directions
from game import Agent
import api
import random
import game
import util
import sys
import os
import csv
import numpy as np
import pandas as pd
"""
    I implemented a classifier using Neural Network and using Decision Tree.
    Decision Tree is not working well on the given dataset therefore I trained my classifier using Neural Network

    Decision Tree code is in method "get_decision_tree" and Neural Network is under the "class NeuralNetwork"

    Training of classifier can be found in "registerInitialState" method

"""

# Neural Network


class NeuralNetwork(object):
    # determine input nodes, hidden nodes , output nodes and learning rate
    # determine Architecture of single layer neural network
    # output_nodes is equal to 4 because we have 4 directions to move
    def __init__(self, input_nodes, hidden_nodes, output_nodes, learning_rate):
        self.inodes = input_nodes
        self.hnodes = hidden_nodes
        self.onodes = output_nodes
        self.lr = learning_rate
        # initialising weight using normal distribution
        # wih - weight from input layer to hidden layer
        self.wih = np.random.normal(0, pow(2 / self.hnodes + self.inodes,
                                           -0.5), (self.hnodes, self.inodes))
        # who - weight from hidden layer to output layer
        self.who = np.random.normal(0, pow(2 / self.onodes + self.hnodes,
                                           -0.5), (self.onodes, self.hnodes))
        self.activation_function = self.sigmoid

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def train(self, inputs_list, targets_list):
        # train = feed forward + back propagation
        # feed forward start
        # calculating weighted sum of input and weight and applying sigmoid activation function
        inputs = np.array(inputs_list, ndmin=2).T
        targets = np.array(targets_list, ndmin=2).T
        hidden_inputs = np.dot(self.wih, inputs)
        hidden_outputs = self.activation_function(hidden_inputs)
        final_inputs = np.dot(self.who, hidden_outputs)
        final_outputs = self.activation_function(final_inputs)
        # feed forward end

        # back propagation start
        # calculating an error (basically, it is derivative of cost function(sum of squares))
        # we have to calculate error contribution of each weight
        # for backpropagation we have to apply chain derivative rule
        output_errors = targets - final_outputs
        hidden_errors = np.dot(self.who.T, output_errors)
        # final_outputs * (1 - final_outputs) --> derivative of sigmoid activation function at output layer
        # output_errors * final_outputs * (1 - final_outputs) --> output_error delta
        # self.lr * np.dot((output_errors * final_outputs * (1 - final_outputs)), hidden_outputs.T) ---> error contribution of weight
        self.who += self.lr * \
            np.dot((output_errors * final_outputs *
                    (1 - final_outputs)), hidden_outputs.T)
        self.wih += self.lr * \
            np.dot((hidden_errors * hidden_outputs *
                    (1 - hidden_outputs)), inputs.T)
        # back propagation ends

    # this method is used for prediction
    # same as feed forward
    def query(self, input_list):
        # convert input list to 2d array
        inputs = np.array(input_list, ndmin=2).T
        hidden_inputs = np.dot(self.wih, inputs)
        hidden_outputs = self.activation_function(hidden_inputs)
        final_inputs = np.dot(self.who, hidden_outputs)
        final_outputs = self.activation_function(final_inputs)
        return final_outputs


# classify method for decision tree
def classify_example(example, tree):
    # root question
    question = list(tree.keys())[0]
    feature_name, comparision_operator, value = question.split()

    # ask question
    if example[feature_name] <= float(value):
        answer = tree[question][0]
    else:
        answer = tree[question][1]

    # base case
    if not isinstance(answer, dict):
        return answer
    # recursive part
    else:
        residual_tree = answer
        return classify_example(example, residual_tree)


# Decision Tree Algorithm


def get_decision_tree(X, y):
    def train_test_split(df, test_size):
        if isinstance(test_size, float):
            test_size = round(test_size * len(df))
        indices = df.index.tolist()

        # test_indices = random.sample(population=indices, k=test_size)
        # for Python 2 Compatibility
        test_indices = np.random.randint(low=min(indices),
                                         high=max(indices),
                                         size=len(indices))

        test_df = df.loc[test_indices]
        train_df = df.drop(test_indices)
        return train_df, test_df

    # Helper Functions

    # Data Pure Check

    def check_purity(data):
        label = data[:, -1]
        unique_classes = np.unique(label)
        if len(unique_classes) == 1:
            return True
        return False

    # Classify

    def classify_data(data):
        label_column = data[:, -1]
        unique_classes, counts_unique_classes = np.unique(label_column,
                                                          return_counts=True)
        if not len(counts_unique_classes):
            return random.choice([1, 2, 3])
        index = counts_unique_classes.argmax()
        classification = unique_classes[index]
        return classification

    # Potential Splits

    def get_potential_splits(data):
        potential_splits = {}
        _, n_columns = data.shape
        for column_index in range(n_columns - 1):
            potential_splits[column_index] = []
            values = data[:, column_index]
            unique_values = np.unique(values)
            # following code is if feature is continuous

            # for index in range(len(unique_values)):
            #     if index == 0:
            #         continue
            #     current_value = unique_values[index]
            #     previous_value = unique_values[index - 1]
            #     p_split = (current_value + previous_value) / 2
            #     potential_splits[column_index].append(p_split)

            # following code if feature is categorical
            potential_splits[column_index] = unique_values
        return potential_splits

    # Split Data

    def split_data(data, split_column, split_value):
        split_column_values = data[:, split_column]
        # check feature or split_column is continuous or categorical
        # below condition is for continuous variable
        # data_below = data[split_column_values <= split_value]
        # data_above = data[split_column_values > split_value]

        # below condition is for categorical variable or encoded categorical variable
        data_below = data[split_column_values == split_value]
        data_above = data[split_column_values != split_value]
        return data_below, data_above

    # Overall Entropy and Determine Best Splits

    def calculate_entropy(data):
        label_column = data[:, -1]
        _, counts = np.unique(label_column, return_counts=True)
        probabilities = counts / float(counts.sum())
        entropy = sum(probabilities * -np.log2(probabilities))
        return entropy

    def calculate_overall_entropy(data_below, data_above):
        n_data_points = len(data_below) + len(data_above)
        # Python 2 Compatibility
        n_data_points = float(n_data_points)
        p_data_below = len(data_below) / n_data_points
        p_data_above = len(data_above) / n_data_points
        overall_entropy = (p_data_below * calculate_entropy(data_below)) + \
            (p_data_above * calculate_entropy(data_above))

        return overall_entropy

    def determine_best_split(data, potential_splits):
        overall_entropy = 999.0
        for columns_index in potential_splits:
            for value in potential_splits[columns_index]:
                data_below, data_above = split_data(data,
                                                    columns_index,
                                                    split_value=value)

                current_overall_entropy = calculate_overall_entropy(
                    data_below, data_above)

                if current_overall_entropy <= overall_entropy:
                    overall_entropy = current_overall_entropy
                    best_split_column = columns_index
                    best_split_value = value

        return best_split_column, best_split_value

    def decision_tree_algorithm(df, counter=0, min_samples=2, max_depth=5):
        if counter == 0:
            global COLUMN_HEADERS
            COLUMN_HEADERS = df.columns
            data = df.values

        else:
            data = df

        # recursive function
        # base case
        if (check_purity(data)) or (len(data) <
                                    min_samples) or counter == max_depth:
            return classify_data(data)
        # recursion start
        else:
            counter += 1

            # helper functions
            potential_splits = get_potential_splits(data)
            split_column, split_value = determine_best_split(
                data, potential_splits)
            data_below, data_above = split_data(data, split_column,
                                                split_value)

            # instantiate sub-tree
            feature_name = COLUMN_HEADERS[split_column]
            # following code of line for if the feature is continuous
            question = "{} <= {}".format(feature_name, split_value)
            # following code of line for if the feature is categorical
            question = "{} = {}".format(feature_name, split_value)

            sub_tree = {question: []}

            # find answers {recursive path of an algorithm}
            yes_answer = decision_tree_algorithm(data_below, counter,
                                                 min_samples, max_depth)
            no_answer = decision_tree_algorithm(data_above, counter,
                                                min_samples, max_depth)

            sub_tree[question].append(yes_answer)
            sub_tree[question].append(no_answer)

            return sub_tree

    def calculate_accuracy(df, tree):
        df["classification"] = df.apply(classify_example,
                                        axis=1,
                                        args=(tree, ))
        df["classification_correct"] = df["classification"] == df["label"]

        accuracy = df["classification_correct"].mean()
        return accuracy

    def main(X, y):

        df = pd.DataFrame(X)
        df.columns = [i for i in list(df.columns)]
        print(df.columns)

        # df.columns = ["Feature_{}".format(c) for c in df.columns]

        # for col in df.columns:
        #     df[col] = df[col].map({0: "ON", 1: "OFF"})
        # print(df.head())

        df["label"] = y
        train_df = df.copy()
        # df = pd.read_csv("Iris.csv")
        # df = df.drop("Id", axis=1)
        # df = df.rename(columns={"species": "label"})
        # train_df, test_df = train_test_split(df, test_size=0.3)
        tree = decision_tree_algorithm(train_df, max_depth=5)
        # # example = test_df.iloc[2]
        # # print(classify_example(example, tree))
        # print(calculate_accuracy(test_df, tree))

        return tree

    return main(X, y)


# ClassifierAgent
#
# An agent that runs a classifier to decide what to do.


class ClassifierAgent(Agent):

    # Constructor. This gets run when the agent starts up.
    def __init__(self):
        print "Initialising"
        self.clf = None

    # Take a string of digits and convert to an array of
    # numbers. Exploits the fact that we know the digits are in the
    # range 0-4.
    #
    # There are undoubtedly more elegant and general ways to do this,
    # exploiting ASCII codes.
    def convertToArray(self, numberString):
        numberArray = []
        for i in range(len(numberString) - 1):
            if numberString[i] == '0':
                numberArray.append(0)
            elif numberString[i] == '1':
                numberArray.append(1)
            elif numberString[i] == '2':
                numberArray.append(2)
            elif numberString[i] == '3':
                numberArray.append(3)
            elif numberString[i] == '4':
                numberArray.append(4)

        return numberArray

    # This gets run on startup. Has access to state information.
    #
    # Here we use it to load the training data.
    def registerInitialState(self, state):

        # open datafile, extract content into an array, and close.
        self.datafile = open('good-moves.txt', 'r')
        content = self.datafile.readlines()
        self.datafile.close()

        # Now extract data, which is in the form of strings, into an
        # array of numbers, and separate into matched data and target
        # variables.
        self.data = []
        self.target = []
        # Turn content into nested lists
        for i in range(len(content)):
            lineAsArray = self.convertToArray(content[i])
            dataline = []
            for j in range(len(lineAsArray) - 1):
                dataline.append(lineAsArray[j])

            self.data.append(dataline)
            targetIndex = len(lineAsArray) - 1
            self.target.append(lineAsArray[targetIndex])

        # data and target are both arrays of arbitrary length.
        #
        # data is an array of arrays of integers (0 or 1) indicating state.
        #
        # target is an array of imtegers 0-3 indicating the action
        # taken in that state.

        # *********************************************
        #
        # Any other code you want to run on startup goes here.
        #
        # You may wish to create your classifier here.
        #
        # *********************************************

        # creating an object of Classifier
        nn = NeuralNetwork(input_nodes=25,
                           hidden_nodes=500,
                           output_nodes=4,
                           learning_rate=0.06)
        epochs = 10
        # training started
        for e in range(epochs):
            for idx, record in enumerate(self.data):
                record = np.asfarray(record)
                # classifier will output the probabilities
                # in training, just maximising the probability of correct target
                # target is one hot encoded
                targets = np.zeros(4) + 0.01
                targets[self.target[idx]] = 0.99
                nn.train(record, targets)
        self.clf = nn

    # Tidy up when Pacman dies

    def final(self, state):

        print "I'm done!"

        # *********************************************
        #
        # Any code you want to run at the end goes here.
        #
        # *********************************************

    # Turn the numbers from the feature set into actions:
    def convertNumberToMove(self, number):
        # print(type(number), number, number == 0)
        if number == 0:
            return Directions.NORTH
        elif number == 1:
            return Directions.EAST
        elif number == 2:
            return Directions.SOUTH
        elif number == 3:
            return Directions.WEST

    # Here we just run the classifier to decide what to do

    def getAction(self, state):

        # How we access the features.
        features = api.getFeatureVector(state)

        # *****************************************************
        #
        # Here you should insert code to call the classifier to
        # decide what to do based on features and use it to decide
        # what action to take.
        #
        # *******************************************************

        # output prediction is represented in the below code.
        f = np.array(features).reshape(1, -1)
        # self.clf is trained in registerInitialState
        pred = self.clf.query(f)
        pred = np.argmax(pred.reshape(-1))
        move = self.convertNumberToMove(pred)
        # Get the actions we can try.
        legal = api.legalActions(state)

        # getAction has to return a move. Here we pass "STOP" to the
        # API to ask Pacman to stay where they are. We need to pass
        # the set of legal moves to teh API so it can do some safety
        # checking.

        return api.makeMove(move, legal)
