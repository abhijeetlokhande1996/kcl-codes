import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
df = pd.read_csv("./dm_data/SMSSpamCollection.csv", encoding="latin-1", header=None)
df.columns = ["Spam/Ham", "Msg"]
ham_count = df["Spam/Ham"].value_counts()["ham"]
spam_count = df["Spam/Ham"].value_counts()["spam"]
print("Number of rows with ham: ", ham_count)
print("Number of rows with spam: ", spam_count)
# Create balanced training data frame

"""
ham_rows_training = df[df["Spam/Ham"] == "ham"]["Msg"][0: spam_count//2]
spam_rows_training = df[df["Spam/Ham"] == "spam"]["Msg"][0: spam_count//2]
training_df = pd.DataFrame({"Msg": []})
training_df["Msg"] = spam_rows_training.append(ham_rows_training)
training_df["Class"] = ["Spam"] * \
    (spam_count // 2) + ["Ham"] * (spam_count // 2)
training_df = training_df.reset_index()
shuffle_index = np.random.randint(
    0, training_df.shape[0], training_df.shape[0])
training_df = training_df.loc[shuffle_index]

print(training_df)

testing_df = pd.DataFrame({"Msg": [], "Class": []})
ham_rows_testing = df[df["Spam/Ham"] == "ham"]["Msg"][spam_count//2:]
spam_rows_testing = df[df["Spam/Ham"] == "spam"]["Msg"][spam_count//2:]
testing_df["Msg"] = spam_rows_testing.append(ham_rows_testing)
print(testing_df)
print(len(ham_rows_testing), len(spam_rows_testing))
_classes = [None] * len(testing_df["Msg"])
_classes[0: len(spam_rows_testing)] = ["Spam"] * len(spam_rows_testing)
# _classes[len(spam_rows_testing):] = ["Ham"] * (len(training_df["Msg"]) - len(spam_rows_testing))
print(len(spam_rows_testing))

"""
X = df["Msg"]
y = df["Spam/Ham"]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

print(X_train.shape)
print(X_test.shape)
print(y_train.shape)
print(y_test.shape)
