import numpy as np
import pandas as pd 
from matplotlib import pyplot as plt

def symmetric_sigmoid_function(x):
    first_term = 2 / (1 + np.exp(-2 * x))
    return first_term - 1
def sigmoid(x):
    return 1 / (1 + np.exp(-x))
def tangent_sigmoid(x):
    return (2 / (1 + np.exp(-2 * x))) - 1
def log_sigmoid_function(x, deriv = False):
#    if deriv:
        # first_term = 1 / (1 + np.exp(-x))
#        return x * (1 - x)
    return 1 / (1 + np.exp(-x))
def q4():
    print("Q4 Output: ")
    X = [np.array([0, 1, 0, 1]), np.array([1,0,1,0]), np.array([0, 0 ,1,1])]
#    w = np.array([[-0.7057, 1.9061, 2.6605, -1.1359], [0.4900, 1.9324, -0.4269, -5.1570], [0.9438, -5.4160, -0.3431, -0.2931]])
#    bias = [4.8432, 0.3973, 2.1761]
    for train in X:
        w = np.array([[-0.7057, 1.9061, 2.6605, -1.1359], [0.4900, 1.9324, -0.4269, -5.1570], [0.9438, -5.4160, -0.3431, -0.2931]])
        bias = np.array([4.8432, 0.3973, 2.1761]).reshape(-1, 1)
        train = train.reshape(-1, 1)
#        print(train)
        op_1 = tangent_sigmoid(np.dot(w, train) + bias)
        
        w = np.array([[-1.1444, 0.3115, -9.9812], [0.0106, 11.5477, 2.6479]])
        bias = np.array([2.5230, 2.6463]).reshape(-1, 1)
        op_2 = log_sigmoid_function(np.dot(w, op_1) + bias)
        
        print(np.round(op_2.reshape(-1)))

def q5():
    print("Q5 answer")
    t = 0.5
    eta = 0.25
    X = np.array([0.1, 0.9]).reshape(-1, 1)
    w1 = np.array([[0.5, 0.3], [-0.7, 0]])
    w2 = np.array([[0.8], [1.6]])
    b1 = 0.2
    b2 = -0.4
    net_1 = np.dot(w1, X) + b1 
    op_1 = symmetric_sigmoid_function(net_1)
    net_2 = np.dot(w2.T, op_1) + b2
    op_2 = symmetric_sigmoid_function(net_2)
    op = op_2[0][0]
    
    op_error = t - op
    op_delta = op_error * log_sigmoid_function(op, deriv=True) 
    
    h1_error = op_delta * w2
    h1_delta = h1_error * log_sigmoid_function(op_1, deriv=True)
    w1 = w1 + eta * h1_delta

    
    w2 = w2 + eta * op_1 * op_delta
    b2 -= eta * op_delta
    for el in h1_delta.reshape(-1):
        b1 -= eta * el
    # print(op_1.reshape(-1), op_2.reshape(-1))
    print("After backpropagation: ")
    print("Weight 1 ")
    print(w1)
    print("Weight 2")
    print(w2)
    print("Bias 1 ", b1," Bias 2",  b2)

def q6():
    print("Q6 question answers: ")
    X = np.array([[0,0], [0,1], [1,0], [1,1]])
    y = [0,1,1,0]
    first_term = np.linalg.inv(np.dot(X.T, X))
    w = first_term.dot(X.T) * y
    for idx, item in enumerate(w.T):
        pred = 1 if np.sum(item * X[idx]) > 0 else 0
        print(pred, end=" ")
    print()

    test = np.array([[0.5, -0.1], [-0.2, 1.2], [0.8, 0.3], [1.8, 0.6]])
    print("--------------------")
    for idx, item in enumerate(w.T):
        pred = 1 if np.sum(item * test[idx]) > 0 else 0
        print(pred, end=" ")
    print()

def q7():
    print("Q7 question Output")
    X = np.array([0.05, 0.2, 0.25, 0.3, 0.4, 0.43, 0.48, 0.6, 0.7, 0.8, 0.9, 0.95])
    y = np.array([0.0863, 0.2662, 0.2362, 0.1687, 0.1260, 0.1756, 0.3290, 0.6694, 0.4573, 0.3320, 0.4063, 0.3535])
    c1 = 0.2
    c2 = 0.6
    c3 = 0.9
    sigma = 0.1
    # plt.plot(X, y)
    # plt.show()
    df = pd.DataFrame({"X":X}, columns=["X"])
    df["Y1(X)"] = np.exp(- (df["X"] - c1) ** 2 / (2 * sigma ** 2))
    df["Y2(X)"] = np.exp(- (df["X"] - c2) ** 2 / (2 * sigma ** 2))
    df["Y3(X)"] = np.exp(- (df["X"] - c3) ** 2 / (2 * sigma ** 2))
    df["Bias"] = [1] * len(X)
    df["target"] = y
    # suppose we calculate weight with close form equation
    w = np.array([0.2660, 0.6649, 0.3990, 0])
    df["pred"] = (df[["Y1(X)", "Y2(X)", "Y3(X)", "Bias"]] * w).sum(axis=1)
    print(df)
    """
    w = 0
    eta = 0.1
    for _ in range(9000):
        for idx, x in enumerate(train):
            pred = w * x
            err = y[idx] - pred
            w += eta * err * x
    print("weight: ", w)
    for idx,x  in enumerate([0.1, 0.35, 0.55, 0.75, 0.9]):
        pred = w * x 
        if pred >= x / 2:
            print(f"{x} belongs to class 1")
        else:
            print(f"{x} belongs to class 0")
    """
if __name__ == '__main__':
    q4()
    q5()
    q6()
    q7()
