from locale import NOEXPR
import numpy as np
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
class Node(object):
	def __init__(self, predicted_class):
		# self.num_samples = num_samples
		# self.num_samples_per_class = num_samples_per_class
		self.predicted_class = predicted_class
		self.feature_index = 0
		self.threshold = 0
		self.right = None
		self.left = None

	def __str__(self):
		return f"feature index {self.feature_index} predicted class {self.predicted_class}"



class DTC(object):
	def __init__(self, depth = 1):
		self.max_depth = depth

	def fit(self, X, y):
		self.n_classes = len(set(y))
		self.n_features = X.shape[1]
		self.tree = self.__build_tree(X, y)
	def __best_split(self, X, y):
		# need minimum to element to split a node

		el_count = y.size
		if el_count <= 1:
			return None, None
		
		# count of each class in the current node
		num_parent_class = [np.sum(y == c) for c in range(self.n_classes)]
		# gini of current node
		best_gini_node = 1 - sum((n / el_count)**2 for n in num_parent_class)
		# best_idx: index of feature for the best split or None if no split is found
		# best_thr: Threshold to use for the split, or None if no split is found
		# "Threshold": for continuous variable threshold is mid point between adjacent points.
		# we are sorting a data 

		best_idx, best_thr = None, None
		for idx in range(self.n_features):
			# sorting a data, input_feature and target
			# first assuming thresholds is instances
			thresholds, classes = zip(*sorted(zip(X[:, idx], y)))

			num_left = [0] * self.n_classes
			num_right = num_parent_class.copy()
			# num_parent_class - contain count of each class in attribute
			# iterating from 1 beacuse for calculating threshold
			# need to consider current as well as previous element
			for i in range(1, el_count):
				# coding possible split condition
				c = classes[i - 1]
				num_left[c] += 1
				num_right[c] -= 1
				# total count of element is el_count
				# if left = i and right = el_count - i
				# left + right = i + el_count - i = el_count
				gini_left = 1 - sum((num_left[x] / i) ** 2 for x in range(self.n_classes))
				gini_right = 1 - sum((num_left[x] / el_count - i) ** 2 for x in range(self.n_classes))
				gini = i * gini_left + (el_count - i) * gini_right

				# no need for this condition but for safe side
				if thresholds[i] == thresholds[i - 1]:
					continue
				# we need smallest gini impurity
				# best_idx = index of the features for best split
				# best_thr = threshold to use for the split
				if gini < best_gini_node:
					best_gini_node = gini
					best_idx = idx
					best_thr = (thresholds[i] + thresholds[i - 1]) / 2
		# best_idx : index of feature for best split
		return best_idx, best_thr

	def __build_tree(self, X, y, depth=0):
		# build decision tree recursively by finding the best split
		# or until it reaches to max depth
		num_samples_per_class = [ np.sum(y == c) for c in range(self.n_classes)]
		predicted_class = np.argmax(num_samples_per_class)
		node = Node(predicted_class=predicted_class)
		if depth < self.max_depth:
			# idx - Index of element of row instance we pick for split
			idx, thr = self.__best_split(X, y)
			if idx:
				# generate left as well as right tree
				left_indices = X[:, idx] < thr
				X_left, y_left = X[left_indices], y[left_indices]
				X_right, y_right = X[~left_indices], y[~left_indices]
				node.feature_index = idx
				node.threshold = thr
				node.left = self.__build_tree(X_left, y_left, depth + 1)
				node.right = self.__build_tree(X_right, y_right, depth + 1)
		return node

	def predict(self, X):
		return [self.__predict(ip) for ip in X]
	
	def __predict(self, ip):
		node = self.tree
		while node.left:
			if ip[node.feature_index] < node.threshold:
				node = node.left
			else:
				node = node.right
		return node.predicted_class



# dataset = load_iris()
# X = dataset["data"]
# y = dataset["target"]
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2)
# dtc = DTC(2)

# dtc.fit(X_train, y_train)
# y_pred = dtc.predict(X_test)
# # Need K-Fold validation
# print("Accuracy Score: ", accuracy_score(y_test, y_pred))
# # dtc.best_split(X, y)

