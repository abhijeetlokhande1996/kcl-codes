from tensorflow import keras
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Conv2D, MaxPool2D, Dense, Flatten, Dropout, BatchNormalization, Activation
from tensorflow.keras.datasets import mnist
import numpy as np
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import KFold
from copy import deepcopy

(xTrain, yTrain), (xTest, yTest) = mnist.load_data()
xTrain = xTrain.astype("float32")
xTest = xTest.astype("float32")

# # shuffle training data
# combined = list(zip(xTrain, yTrain))
# np.random.shuffle(combined)
# xTrain[:], yTrain[:] = zip(*combined)
# # shuffle test data
# combined = list(zip(xTest, yTest))
# np.random.shuffle(combined)
# xTest[:], yTest[:] = zip(*combined)


xTrain /= xTrain.max()
xTest /= xTest.max()

xTrain = xTrain.reshape(xTrain.shape[0], xTrain.shape[1], xTrain.shape[1], 1)
xTest = xTest.reshape(xTest.shape[0], xTest.shape[1], xTest.shape[1], 1)
datagen = ImageDataGenerator(
    featurewise_center=False,  # set input mean to 0 over the dataset
    samplewise_center=False,  # set each sample mean to 0
    featurewise_std_normalization=False,  # divide inputs by std of the dataset
    samplewise_std_normalization=False,  # divide each input by its std
    zca_whitening=False,  # apply ZCA whitening
    # randomly rotate images in the range (degrees, 0 to 180)
    rotation_range=10,
    zoom_range=0.1,  # Randomly zoom image
    # randomly shift images horizontally (fraction of total width)
    width_shift_range=0.1,
    # randomly shift images vertically (fraction of total height)
    height_shift_range=0.1,
    horizontal_flip=False,  # randomly flip images
    vertical_flip=False)  # randomly flip images

datagen.fit(xTrain)

inputShape = xTrain[0].shape


# X = np.append(xTrain, xTest, axis=0)
# Y = np.append(yTrain, yTest, axis=0)
X = xTrain
Y = yTrain


model = Sequential()
model.add(Conv2D(28, (5, 5), padding='same', input_shape=xTrain.shape[1:]))
model.add(Activation('relu'))
model.add(BatchNormalization())
model.add(Conv2D(28, (5, 5)))
model.add(Activation('relu'))
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(0.25))  # original dropout 0.25
model.add(Conv2D(32, (5, 5), padding='same', input_shape=xTrain.shape[1:]))
model.add(Activation('relu'))
model.add(BatchNormalization())

model.add(Conv2D(32, (5, 5)))
model.add(Activation('relu'))
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(0.35))  # original value 0.35 -> 0.25

model.add(Flatten())
model.add(Dense(1000))
model.add(Activation('relu'))
model.add(Dropout(0.45))  # original value 0.45 -> 0.25
model.add(Dense(10))
model.add(Activation('softmax'))
# optimizer = "adam" (original)
model.compile(loss="sparse_categorical_crossentropy",
              optimizer="adam", metrics=["accuracy"])
callbacks = [EarlyStopping(monitor='val_loss', patience=2), ModelCheckpoint(
    filepath='best_model_mnist.h5', monitor='val_loss', save_best_only=True)]
# callbacks = callbacks
history = model.fit(xTrain, yTrain, batch_size=128, epochs=20,
                    verbose=1, validation_data=(xTest, yTest), callbacks=callbacks)

# model.save("final_mnist_model.h5")
