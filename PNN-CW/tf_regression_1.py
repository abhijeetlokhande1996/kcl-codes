import numpy as np
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
import tensorflow as tf
import pandas as pd
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = "2"


x, y = make_regression(n_samples=1000, n_features=5, noise=0)
w = tf.random.normal((x.shape[1], 1), dtype=tf.float64)
b = 0
xTrain, xTest, yTrain, yTest = train_test_split(x, y, test_size = 0.3, random_state = 0)
yTrain = yTrain.reshape(yTrain.shape[0], 1)
yTest = yTest.reshape(yTest.shape[0], 1)
print(f"Shapes: xTrain {xTrain.shape} xTest {xTest.shape} yTrain {yTrain.shape} yTest {yTest.shape}")
learningRate = 0.0001
epochs = 100
for _ in range(epochs):
    yPred = xTrain @ w + b
    _error = yPred - yTrain.reshape(yTrain.shape[0], 1)
    w -= learningRate * (tf.transpose(xTrain) @ _error)
    for e in _error:
        b -= learningRate * e


yPred = [] 
for test in xTest:
    test = test.reshape(test.shape[0], 1)
    pred = tf.transpose(w) @ test + b
    yPred.append(pred.numpy()[0][0])

print("R2 Score: ", r2_score(yTest, yPred))
resultDf = pd.DataFrame({"yTest": yTest.reshape(-1), "yPred": yPred})
print(resultDf.head())
