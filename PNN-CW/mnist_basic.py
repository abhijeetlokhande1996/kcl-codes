import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical


tf.compat.v1.disable_v2_behavior()


def initWeights(shape):
	initRandomDist = tf.compat.v1.truncated_normal(shape, stddev=0.1)
	return tf.Variable(initRandomDist)

def initBias(shape):
	initBiasVal = tf.constant(0.1, shape=shape)
	return tf.Variable(initBiasVal)

def conv2d(x, W):
	# x -> actual input tensor -> shape [batch, H, W, Channels]
	# W -> Shape -> [filter H, filter W, Channel In, Channels OUT]
	return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME')

def maxPooling2by2(x):
	# x --> shape [batch, H, W, C]
	return tf.nn.max_pool(x, ksize=[1,2,2,1], strides=[1,2,2,1], padding="SAME")

def convolutionLayer(inputX, shape):
	W = initWeights(shape)
	b = initBias([shape[3]])
	return tf.nn.relu(conv2d(inputX, W) + b)
def normalFullLayer(inputLayer, size):
	inputSize = int(inputLayer.get_shape()[1])
	W = initWeights([inputSize, size])
	b = initBias([size])
	return tf.matmul(inputLayer, W) + b

def main():

	(xTrain, yTrain), (xTest, yTest) = mnist.load_data()


	xTrain = xTrain.astype("float32")
	xTest = xTest.astype("float32")

	# scale the data
	xTrain /= 255
	xTest /= 255

	# one hot encoding of class labels

	yTrain = to_categorical(yTrain, 10)
	yTest = to_categorical(yTest, 10)


	# shape transformation for CNN
	xTrain = xTrain.reshape(xTrain.shape[0], 28, 28, 1)
	xTest = xTest.reshape(xTest.shape[0], 28, 28, 1)

	# Placeholders
	x = tf.compat.v1.placeholder(tf.float32, shape=[None, 784])
	yTrue = tf.compat.v1.placeholder(tf.float32, shape=[None, 10])

	# Layers
	xImage = tf.reshape(x, [-1, 28, 28, 1])
	convo1 = convolutionLayer(xImage, shape=[5, 5, 1, 32])
	convo1Pooling = maxPooling2by2(convo1)

	convo2 = convolutionLayer(convo1Pooling, shape=[5, 5, 32, 64])
	convo2Pooling = maxPooling2by2(convo2)

	convo2Flat = tf.reshape(convo2Pooling, [-1, 7*7*64])
	fullLayer1 = tf.nn.relu(normalFullLayer(convo2Flat, 1024))

	# Dropout
	holdProb = tf.compat.v1.placeholder(tf.float32)
	fullOneDropout = tf.compat.v1.nn.dropout(fullLayer1, keep_prob=holdProb)
	yPred = normalFullLayer(fullOneDropout, 10)

	# Loss function
	crossEntropy = tf.compat.v1.reduce_mean(tf.compat.v1.nn.softmax_cross_entropy_with_logits(labels=yTrue, logits=yPred))
	# Optimizer
	optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate=0.001)
	train = optimizer.minimize(crossEntropy)

	init = tf.compat.v1.global_variables_initializer()

	steps = 1
	with tf.compat.v1.Session() as sess:
		sess.run(init)
		sess.run(train, feed_dict= {x: xTrain, yTrue: yTrain, holdProb: 0.5})
		matches = tf.equal(tf.argmax(yPred, 1), tf.argmax(yTrue, 1))
		acc = tf.reduce_mean(tf.cast(matches, tf.float32))
		print(sess.run(acc, feed_dict={x: xTest, yTrue: yTest, holdProb:1}))

main()