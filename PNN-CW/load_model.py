from emnist import extract_test_samples
import numpy as np
import tensorflow as tf
from tensorflow.keras import models
from tensorflow.keras.datasets import mnist
import cv2
import os
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.image import ImageDataGenerator

# Path ./own_images/2828_my_own_2.png
# final_mnist_model_final.h5
mnist_model = tf.keras.models.load_model(
    "./h5/Selvakumar_GabrielDevaKiruba.h5")
# mnist_model = tf.keras.models.load_model(
#     "./h5/BEST_MNIST_LAZY_PROGRAMMER.h5")
# mnist_model_1.h5

(xTrain, yTrain), (xTest, yTest) = mnist.load_data()

X = np.append(xTrain, xTest, axis=0)
Y = np.append(yTrain, yTest, axis=0)
datagen = ImageDataGenerator(
    featurewise_center=False,  # set input mean to 0 over the dataset
    samplewise_center=False,  # set each sample mean to 0
    featurewise_std_normalization=False,  # divide inputs by std of the dataset
    samplewise_std_normalization=False,  # divide each input by its std
    zca_whitening=False,  # apply ZCA whitening
    # randomly rotate images in the range (degrees, 0 to 180)
    rotation_range=20,  # original 10 -> 20
    zoom_range=0.1,  # Randomly zoom image
    # randomly shift images horizontally (fraction of total width)
    width_shift_range=0.1,
    # randomly shift images vertically (fraction of total height)
    height_shift_range=0.1,
    horizontal_flip=False,  # randomly flip images
    vertical_flip=False)  # randomly flip images


shuffleIndex = np.random.randint(
    low=0, high=xTrain.shape[0], size=xTrain.shape[0])
X = X[shuffleIndex]
Y = Y[shuffleIndex]
_, x_test, _, y_test = train_test_split(X, Y, test_size=0.99)
datagen.fit(x_test.reshape(x_test.shape[0], 28, 28, 1))
print("Model evaluate on (xTest)")
"""
xTest = xTest.reshape(xTest.shape[0], xTest.shape[1], xTest.shape[1], 1)
print(xTest.shape, yTest.shape)
"""
# x_test = x_test.reshape(x_test.shape[0], x_test.shape[1], x_test.shape[1], 1)
# mnist_model.evaluate(x_test, y_test)


"""
# code required when spratling model is loaded
outputs = mnist_model.predict(xTest)
labels_predicted = np.argmax(outputs, axis=1)
misclassified = sum(labels_predicted != yTest)
print('Percentage misclassified = ', 1 - (misclassified/yTest.size))
"""


# print("Model evaluate on xTrain")
# mnist_model.evaluate(xTrain.reshape(
#     xTrain.shape[0], xTrain.shape[1], xTrain.shape[1], 1), yTrain)

images_list = os.listdir("./own_images/")

for img in images_list:
    try:
        # ./own_images/2828_my_own_2.png
        gray = cv2.imread(f"./own_images/{img}", cv2.IMREAD_GRAYSCALE)
        gray = cv2.resize(255-gray, (28, 28))
        # gray = gray / 255.00
        gray = gray.reshape(1, gray.shape[0], gray.shape[0], 1)
        print(img, np.argmax(mnist_model.predict(gray).reshape(-1)))
    except:
        # print(f"Error on {img}")
        pass

# images, labels = extract_test_samples('digits')
# images = images.reshape(images.shape[0], images.shape[1], images.shape[1], 1)
# print(mnist_model.evaluate(images, labels))
