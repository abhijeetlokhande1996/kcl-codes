import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = "2"
os.environ['KMP_DUPLICATE_LIB_OK']='True'

import tensorflow as tf

# Initialization of Tensors
# shape can be omitted
# shape can also work like reshape in numpy
x = tf.constant(4.0, shape=(2,3))

x = tf.constant([[1,2,3], [4,5,6]])
print(x)
ones = tf.ones(shape=(4,4))
print(ones)
zeros = tf.zeros(shape=(4,3))
print(zeros)
eye = tf.eye(num_rows=2)
print(eye)
normalDist = tf.random.normal((3,3), mean=0, stddev=1)
print("Normal Dist: ", normalDist)
uniformDist = tf.random.uniform((3,3), minval=0, maxval=1)
print("Uniform Dist: ", uniformDist)
_range = tf.range(start=1, limit=20, delta=2)
print(_range)
# casting
_range = tf.cast(x, dtype=tf.float64)
print("After Casting: ", _range)

# Mathematical Operations
x = tf.constant([1,2,3])
y = tf.constant([9, 8, 7])
z = tf.add(x, y)
# substract, multiply
print(z.shape)
z = x * y
print("Element wise mult: ", z)
z = tf.tensordot(x, y, axes = 1)
print(z)
z = tf.reduce_sum(x * y, axis = 0)
print("Z: ", z)
z = x ** 2
print("Sequare: ", z)
x = tf.random.normal((2,3))
y = tf.random.normal((3,2))
print(x @ y)
print(tf.matmul(x, y))

# Indexing
x = tf.constant([0, 1, 1, 2, 3, 4, 5])
print(x[1:4])
x = tf.random.normal((2,3))
print(x[::2])
indices = tf.constant([0, 3])
x = tf.constant([[1,2,3], [4,5,6]])
print(x[0:1, 0])
# Reshape
x = tf.range(9)
x = tf.reshape(x, (3, 3))
print("Original: ", x)
print(tf.transpose(x))