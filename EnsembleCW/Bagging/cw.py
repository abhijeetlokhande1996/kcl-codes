import numpy as np
import pandas as pd
from sklearn.datasets import make_circles, make_classification
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
import seaborn as sns
sns.set()

if __name__ == "__main__":
    (X, y) = make_classification(n_samples=100, n_classes=2,
                                 n_features=2, n_informative=2, n_redundant=0, n_clusters_per_class=1)
    df = pd.DataFrame(
        {"Feature 1": X[:, 0], "Feature 2": X[:, 1], "Class": y}, index=range(X.shape[0]))

    print(df.head())
    """
    data_class_1 = df[df["Class"] == 0][[
        "Feature 1", "Feature 2"]].reset_index(drop=True).values
    data_class_2 = df[df["Class"] == 1][[
        "Feature 1", "Feature 2"]].reset_index(drop=True).values

    row_len = data_class_1.shape[0]
    for idx in range(row_len):
        data_class_1[idx][1] = data_class_1[idx][1] ** 2
        data_class_2[idx][1] = data_class_2[idx][1] ** 2

        data_class_1[idx][0] = data_class_1[idx][0] ** 0.5
        data_class_2[idx][0] = data_class_2[idx][0] ** 0.5

    y = [1] * data_class_1.shape[0]
    y.extend([-1] * data_class_2.shape[0])
    X = np.append(data_class_1, data_class_2, axis=0)
    df = pd.DataFrame(data=X, columns=["Feature 1", "Feature 2"])
    df["Class"] = y
    """
    sns.scatterplot(x="Feature 1", y="Feature 2", data=df,
                    hue=df["Class"], palette=["red", "green"])

    plt.show()
