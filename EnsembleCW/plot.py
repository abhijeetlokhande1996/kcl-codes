import itertools
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
sns.set()
mks = itertools.cycle(['x', 'o'])

x1 = np.array([[4, 0], [4, 1], [4, 2], [4, 3], [4, 4], [5, 4], [6, 4], [7, 4], [8, 4], [8, 3], [
              8, 2], [8, 1], [8, 0], [8, -1], [8, -2], [7, -2], [6, -2], [5, -2], [4, -2], [4, -1]])

x2 = np.array([[2, 0], [2, 1], [2, 2], [2, 3], [2, 4], [2, 5], [2, 6], [3, 6], [4, 6], [5, 6], [6, 6], [7, 6], [8, 6], [9, 6], [10, 6], [10, 5], [10, 4], [
              10, 3], [10, 2], [10, 1], [10, 0], [10, -1], [10, -2], [10, -3], [9, -3], [8, -3], [7, -3], [6, -3], [5, -3], [4, -3], [3, -3], [2, -3], [2, -2], [2, -1]])


X = np.append(x1, x2, axis=0)
y = [1] * x1.shape[0]
y.extend([-1] * x2.shape[0])
df = pd.DataFrame(
    {"Feature 1": X[:, 0], "Feature 2": X[:, 1], "Class": y}, index=range(X.shape[0]))


sns_plot = sns.lmplot(data=df, x="Feature 1", y="Feature 2",
                      hue="Class", palette=["red", "green"], markers=['x', 'o'], scatter=True, fit_reg=False)
# sns_plot.figure.savefig("output.png")
plt.show()
