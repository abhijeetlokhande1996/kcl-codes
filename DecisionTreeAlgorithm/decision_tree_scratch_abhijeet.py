import numpy as np
import pandas as pd

import random

# Train-Test-Split


def train_test_split(df, test_size):
    if isinstance(test_size, float):
        test_size = round(test_size * len(df))
    indices = df.index.tolist()

    # test_indices = random.sample(population=indices, k=test_size)
    # for Python 2 Compatibility
    test_indices = np.random.randint(
        low=min(indices), high=max(indices), size=len(indices))

    test_df = df.loc[test_indices]
    train_df = df.drop(test_indices)
    return train_df, test_df

# Helper Functions

# Data Pure Check


def check_purity(data):
    label = data[:, -1]
    unique_classes = np.unique(label)
    if len(unique_classes) == 1:
        return True
    return False

# Classify


def classify_data(data):
    label_column = data[:, -1]
    unique_classes, counts_unique_classes = np.unique(
        label_column, return_counts=True)
    index = counts_unique_classes.argmax()
    classification = unique_classes[index]
    return classification

# Potential Splits


def get_potential_splits(data):
    potential_splits = {}
    _, n_columns = data.shape
    for column_index in range(n_columns - 1):
        potential_splits[column_index] = []
        values = data[:, column_index]
        unique_values = np.unique(values)

        for index in range(len(unique_values)):
            if index == 0:
                continue
            current_value = unique_values[index]
            previous_value = unique_values[index - 1]
            p_split = (current_value + previous_value) / 2
            potential_splits[column_index].append(p_split)
    return potential_splits

# Split Data


def split_data(data, split_column, split_value):
    split_column_values = data[:, split_column]
    data_below = data[split_column_values <= split_value]
    data_above = data[split_column_values > split_value]
    return data_below, data_above

# Overall Entropy and Determine Best Splits


def calculate_entropy(data):
    label_column = data[:, -1]
    _, counts = np.unique(label_column, return_counts=True)
    probabilities = counts / float(counts.sum())
    entropy = sum(probabilities * -np.log2(probabilities))
    return entropy


def calculate_overall_entropy(data_below, data_above):
    n_data_points = len(data_below) + len(data_above)
    # Python 2 Compatibility
    n_data_points = float(n_data_points)
    p_data_below = len(data_below) / n_data_points
    p_data_above = len(data_above) / n_data_points
    overall_entropy = (p_data_below * calculate_entropy(data_below)) + \
        (p_data_above * calculate_entropy(data_above))
    return overall_entropy


def determine_best_split(data, potential_splits):
    overall_entropy = 999
    for columns_index in potential_splits:
        for value in potential_splits[columns_index]:
            data_below, data_above = split_data(
                data, columns_index, split_value=value)

            current_overall_entropy = calculate_overall_entropy(
                data_below, data_above)

            if current_overall_entropy <= overall_entropy:
                overall_entropy = current_overall_entropy
                best_split_column = columns_index
                best_split_value = value

    return best_split_column, best_split_value


def get_feature_types(df):
    feature_types = []
    for col in df.columns:
        unique_values = df[col].unique()
        if len(unique_values) < 15 or isinstance(unique_values[0], str):
            feature_types.append("categorical")
        else:
            feature_types.append("continuous")
    return feature_types

def decision_tree_algorithm(df, counter=0, min_samples=2, max_depth=5):
    if counter == 0:
        global COLUMN_HEADERS
        COLUMN_HEADERS = df.columns
        data = df.values
        FEATURE_TYPES = get_feature_types(df)

    else:
        data = df

    # recursive function
    # base case
    if (check_purity(data)) or (len(data) < min_samples) or counter == max_depth:
        return classify_data(data)
    # recursion start
    else:
        counter += 1

        # helper functions
        potential_splits = get_potential_splits(data)
        split_column, split_value = determine_best_split(
            data, potential_splits)
        data_below, data_above = split_data(
            data, split_column, split_value)

        # instantiate sub-tree
        feature_name = COLUMN_HEADERS[split_column]
        if FEATURE_TYPES != "categorical":
            question = "{} <= {}".format(feature_name, split_value)
        else:
            question = "{} = {}".format(feature_name, split_value)

        sub_tree = {question: []}

        # find answers {recursive path of an algorithm}
        yes_answer = decision_tree_algorithm(
            data_below, counter, min_samples, max_depth)
        no_answer = decision_tree_algorithm(
            data_above, counter, min_samples, max_depth)

        sub_tree[question].append(yes_answer)
        sub_tree[question].append(no_answer)

        return sub_tree


def classify_example(example, tree):
    # root question
    question = list(tree.keys())[0]
    feature_name, comparision_operator, value = question.split()

    # ask question
    if example[feature_name] <= float(value):
        answer = tree[question][0]
    else:
        answer = tree[question][1]

    # base case
    if not isinstance(answer, dict):
        return answer
    # recursive part
    else:
        residual_tree = answer
        return classify_example(example, residual_tree)


def calculate_accuracy(df, tree):
    df["classification"] = df.apply(classify_example, axis=1, args=(tree,))
    df["classification_correct"] = df["classification"] == df["label"]

    accuracy = df["classification_correct"].mean()
    return accuracy


if __name__ == "__main__":
    df = pd.read_csv("Iris.csv")
    df = df.drop("Id", axis=1)
    df = df.rename(columns={"species": "label"})
    train_df, test_df = train_test_split(df, test_size=0.3)
    tree = decision_tree_algorithm(train_df, max_depth=35)
    example = test_df.iloc[2]
    print(example)
    # print(classify_example(example, tree))
    print(calculate_accuracy(test_df, tree))
