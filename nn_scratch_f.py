import numpy as np
from scipy.special import expit
import pandas as pd


class NeuralNetwork(object):
    def __init__(self, inputnodes, hiddennodes, outputnodes, learningrate) -> None:
        super().__init__()
        self.inodes = inputnodes
        self.hnodes = hiddennodes
        self.onodes = outputnodes
        self.lr = learningrate

        # wih - weight (input to hidden)
        # who - weight (hidden to output)
        self.wih = np.random.normal(
            0, pow(self.hnodes, -0.5), (self.hnodes, self.inodes))
        self.who = np.random.normal(
            0, pow(self.onodes, -0.5), (self.onodes, self.hnodes))
        self.activation_function = lambda x: expit(x)

    def train(self, inputs_list, targets_list):

        inputs = np.array(inputs_list, ndmin=2).T
        targets = np.array(targets_list, ndmin=2).T

        hidden_inputs = np.dot(self.wih, inputs)
        hidden_outputs = self.activation_function(hidden_inputs)

        final_inputs = np.dot(self.who, hidden_outputs)
        final_outputs = self.activation_function(final_inputs)

        output_errors = targets - final_outputs
        hidden_errors = np.dot(self.who.T, output_errors)
        self.who += self.lr * \
            np.dot((output_errors * final_outputs *
                    (1 - final_outputs)), hidden_outputs.T)

        self.wih += self.lr * np.dot((hidden_errors * hidden_outputs *
                                      (1 - hidden_outputs)), inputs.T)

    def query(self, input_list):
        # convert input list to 2d array
        inputs = np.array(input_list, ndmin=2).T
        hidden_inputs = np.dot(self.wih, inputs)
        hidden_outputs = self.activation_function(hidden_inputs)

        final_inputs = np.dot(self.who, hidden_outputs)
        final_outputs = self.activation_function(final_inputs)
        return final_outputs


input_nodes = 784
hidden_nodes = 150
output_nodes = 10

learning_rate = 0.1
nn = NeuralNetwork(inputnodes=input_nodes, hiddennodes=hidden_nodes,
                   outputnodes=output_nodes, learningrate=learning_rate)

train = pd.read_csv("./mnist_train.csv", header=None)
# all_values = train.values
epochs = 3

for e in range(epochs):
    for record in train.values:
        all_values = record
        inputs = (np.asfarray(all_values[1:]) / 255 * 0.99) + 0.01
        targets = np.zeros(output_nodes) + 0.01
        targets[int(all_values[0])] = 0.99
        nn.train(inputs, targets)


test = pd.read_csv("./mnist_test.csv").values
scorecard = []
for record in test:
    all_values = record
    correct_label = int(all_values[0])
    inputs = ((np.asfarray(all_values[1:])) / 255.0 * 0.99) + 0.01
    outputs = nn.query(inputs)
    label = np.argmax(outputs)
    if label == correct_label:
        scorecard.append(1)
    else:
        scorecard.append(0)

scorecard_array = np.asfarray(scorecard)
print("Performance = ", scorecard_array.sum() / scorecard_array.size)
