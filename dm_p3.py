from sklearn.datasets import load_iris
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
def get_most_f(input_feature):
    _dict = {}
    for idx, el in enumerate(input_feature):
        t = target[idx]
        if not el in _dict:
            _dict[el]= {}
        if not t in _dict[el]:
            _dict[el] = [0, 0, 0]
        
        _dict[el][t] += 1

    # calculate most frequest class associated with key
#    for k1, v1 in _dict.items():
        # print(key, item)
#        f_class = None
#        max_freq = -99
#        for k2, v2 in _dict[k1].items():
#            if max_freq < v2:
#                max_freq = v2
#                f_class = k2
            #print(k2, v2)
#        _dict[k1] = {"most_frequent": f_class}
#print(sl_dict)
    return _dict
iris = load_iris()
data = iris['data']
target = iris['target']
target_names = iris['target_names']
sepal_length = data[:,0]
sl_dict = get_most_f(sepal_length)
sepal_width = data[:,1]
sw_dict = get_most_f(sepal_width)

_final_dict = {"sepal_length": [], "sepal_width": [], "class":[]}

for k1, v1 in sl_dict.items():
    for k2, v2 in sw_dict.items():
        _final_dict["sepal_length"].append(k1)
        _final_dict["sepal_width"].append(k2)
        v = [0] * 3
#        max_v1 = max(v1)
#        max_v1 = v1.index(max_v1)
#        max_v2 = max(v2)
#        max_v2 = v2.index(max_v2)
        for idx in range(len(v)):
            v[idx] = v1[idx] + v2[idx]
        max_v = max(v)
        idx = v.index(max_v)
        _final_dict["class"].append(idx)

df = pd.DataFrame(_final_dict)
# print(df.sample(25))

print("------ Test --------")
test = data[:,0:2]
pred = []
for x in test:
    sepal_length = x[0]
    sepal_width = x[1]
    for idx, row in df.iterrows():
        #print(row)
        #break
        if row["sepal_length"] == sepal_length and row["sepal_width"] == sepal_width:
            pred.append(row["class"])
    #break
# print(iris["feature_names"])

print("Score: ", accuracy_score(target, pred) * 100 )
