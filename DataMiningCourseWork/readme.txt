Source code follows object-oriented design pattern. In classification and clustering, I am loading a data, removing unnecessary columns and replacing nan values with np.nan in “load_data_set” function. 

 
Classification: 

“print_info” function print basic information about dataset. 

“fit_return_clf” function encoding dataset with Label encoder, printing discrete values of each column, usinf kFold validation training a model and printing accuracy of the model. 

“test” function created three datasets d_prime, d1_prime, d2_prime according to coursework pdf. It builds two models, first from d1_prime dataset and second from d2_prime dataset and testing 2 models on d_prime to print error rate 

. 

Cluster: 

“plot_cluster” function plots the 15 scatter plot  from each pair column of the dataset using Kmeans algorithm and num of cluster is 3. 

“calculate_matrix” functions calculate within cluster distance and between cluster distance for k=3, k=5 and k=10 

“calculate_distance” helper function to calculate ecludian distance between 2 points. 

“within_cluster_distance” helper function to calculate within cluster distance 