import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder, OrdinalEncoder
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import KFold, train_test_split
from sklearn.cluster import KMeans
from matplotlib import pyplot as plt
class Classification(object):
	@staticmethod
	def load_data_set():
		adult = pd.read_csv("./data/adult.csv", encoding="utf-8")
		adult = adult.fillna(np.nan)
		adult = adult.drop(["fnlwgt"], axis=1)

		return adult
	def __init__(self):
		self.adult = Classification.load_data_set()
		print("Printing classification output")
		self.print_info(self.adult.copy())
        # _adult is encoded with Label Encoder and nan values is intact in _adult
		_adult = self.encode_print_discrete_values(self.adult.copy())
		self.fit_return_clf(_adult.copy())
		self.test(self.adult.copy(), _adult.copy())
	def print_info(self, adult):
		_sum = 0
		for col in adult.columns:
			_sum += adult.isna().sum()[col]
		# print("Number of missing values: ", _sum)
		# print("Fraction of missing values over all attribute values: ", _sum / adult.size)
		_count = 0
		for idx, el in adult.isna().sum(axis=1).items():
			if el > 0:
				_count += 1
		# print("Number of instance with missing values: ", _count)
		# print("Fraction of instances with missing values over all instances: ", _count / adult.shape[0])
		op_df = pd.Series({"number_of_instance": len(adult.index), "number_of_missing_values": _sum, "fraction_of_instance_with_missing_over_all_instances":_sum / adult.size, "number_of_instances_with_missing_value": _count, "fraction_of_instances_with_missing_values_over_all_instances":_count / adult.shape[0]}).to_frame()
		op_df.columns = ["Count"]
		print(op_df)
	def encode_print_discrete_values(self, adult):
		# Label Encoder
		# Ignoring the nan values and rest of the values are encoding
		adult = adult.apply(lambda s: pd.Series( LabelEncoder().fit_transform(s[s.notnull()]), index=s[s.notnull()].index ))
		for col in adult.columns:
			print(f"Discrete values of {col}: ", col)
			# ignoring nan values while printing discrete values
			print(np.unique(adult[col][~np.isnan(adult[col])]))
		# print(_adult[col].unique())
		return adult
	def fit_return_clf(self, _adult):
		# dropping nan values for training
		_adult = _adult.dropna()
		y = _adult["class"]
		_adult = _adult.drop(["class"], axis=1)
		"""
		_adult dataframe with no missing values in the instance
		data in dataset is not large. Kfold cross validation is good choice to apply rather train_test_split
		"""
		max_score = 0
		f_clf = None
		kf = KFold(n_splits=10, shuffle=True)
		clf = DecisionTreeClassifier()

		for i in range(10):
			train_test_index = next(kf.split(_adult))
			X_train = _adult.iloc[train_test_index[0]]
			y_train = y.iloc[train_test_index[0]]
			X_test = _adult.iloc[train_test_index[1]]
			y_test = y.iloc[train_test_index[1]]
			clf.fit(X_train, y_train)
			score = clf.score(X_test, y_test)
			if i == 0:
				max_score = score
				f_clf = clf
			else:
				if max_score < score:
					max_score = score
					f_clf = clf

		print("Maximum score on decision tree model on KFold cross validation is: ", max_score)
		return f_clf

	def test(self, adult, _adult):
		# adult dataset is no LabelEncoded
		adult_missing_val =  adult[adult.isna().any(axis='columns')]
		adult_no_missing_val = adult.dropna().sample(len(adult_missing_val.index))

		print("DISCRETE TEST: ")
		self.encode_print_discrete_values(adult_no_missing_val)

		# d_prime is made from adult_missing_val which contain missing values 
		# and adult_no_missing_val which don't contain missing values
		d_prime = adult_missing_val.append(adult_no_missing_val)
		d1_prime = d_prime.fillna("missing")
		# d1_prime = d_prime.dropna()
		d2_prime = d_prime.copy()
		# replacing missing values with mode
		for col in d2_prime:
			if d2_prime[col].isna().sum() > 0:
				_mode = d2_prime[col].mode()[0]     
				d2_prime[col] = d2_prime[col].fillna(_mode)

		# encode data in d1_prime (ordinal encoder is same as label encoded)
		target_d1_prime = d1_prime["class"]
		d1_prime = d1_prime.drop(["class"], axis=1)
		
		ord_encoder = OrdinalEncoder()
		encoded_data = ord_encoder.fit_transform(d1_prime[d1_prime.columns])

		X_train, X_test, y_train, y_test = train_test_split(encoded_data, target_d1_prime, test_size=0.2)
		clf_d1_prime = DecisionTreeClassifier()
		clf_d1_prime.fit(X_train, y_train)
		print("Accuracy of Decision Tree train from d1_prime train data  on d1_prime test data: ", clf_d1_prime.score(X_test, y_test))

		# Decision Tree making on d2_prime
		target_d2_prime = d2_prime["class"]
		d2_prime = d2_prime.drop(["class"], axis=1)


		ord_encoder = OrdinalEncoder()
		encoded_data = ord_encoder.fit_transform(d2_prime[d2_prime.columns])

		X_train, X_test, y_train, y_test = train_test_split(encoded_data, target_d2_prime, test_size=0.2)

		clf_d2_prime = DecisionTreeClassifier()
		clf_d2_prime.fit(X_train, y_train)

		print("Accuracy of Decision Tree train from d2_prime train data on d2_prime test data: ", clf_d2_prime.score(X_test, y_test))

		# targe variable in _adult is 0 or 1
		# but we need <50 and >50 target variable because d1_prime and d2_prime models uses on <50 and >50 class label
		# getting samples from _adult
		# when we performed dropna operation on _adult  we did not perform reset index 
		# therefore adult df index is super set of _adult index
		# we are getting actual class label from adult df and we picking it up by index of data which we already sliced from _adult

		# Original dataset D (self.adult) contain many nan or missing values
		# we need to remove missing values and encode categorical values to number
		# therefore we will use _adult dataset which already encoded 
		# only thing is we need to remove missing values from )adult

		
		test_dataset = _adult.dropna()
		test_data = test_dataset.drop(["class"], axis=1).sample(2000, random_state = 1)
		# we did not encode target variable
		# therefore we need to use original class label (<50k and >50k)
		# due to this we get class label from adult (copy of original dataset) 
		test_result = adult["class"][test_data.index]

		score_d1_prime = clf_d1_prime.score(test_data, test_result)
		score_d2_prime = clf_d2_prime.score(test_data, test_result)
		print("Error rate of  d1_prime  and d2_prime classifier on original dataset", 1 - score_d1_prime, 1 - score_d2_prime)
        

class Cluster(object):
	@staticmethod
	def load_data_set():
		df = pd.read_csv("./data/wholesale_customers.csv")
		df = df.drop(["Channel", "Region"], axis=1)
		df = df.fillna(np.nan)
		return df
	def __init__(self):
		self.df = Cluster.load_data_set()
		print("Printing cluster output")
		self.print_info(self.df.copy())
		self.plot_cluster(self.df.copy())
		self.calculate_matrix(self.df.copy())

	def print_info(self, df):
		info_dict = {}
		for col in df.columns:
			info_dict[col] = [df[col].mean(), df[col].min(), df[col].max(), f"[{df[col].min()} to {df[col].max()}]"]
		info_df = pd.DataFrame(info_dict, index=["Mean", "Min", "Max", "Range"])
		print(info_df.T)
	def calculate_distance(self, pl1, pl2):
		distance = 0
		for idx in range(len(pl1)):
			p1 = pl1[idx]
			p2 = pl2[idx]
			distance += (p1 - p2) ** 2
		return distance

	def within_cluster_distance(self, center, data):
		center = list(center)
		distance = 0
		for instance in data:
			distance += self.calculate_distance(center, instance)

		# print("Within cluster Distance ", distance)
		return distance

	def plot_cluster(self, df):
		x = df.values
		# , init="k-means++", max_iter=500, n_init=10,
		kmeans = KMeans(n_clusters=3, random_state=0)
		y_kmeans = kmeans.fit_predict(x)
		clusters = np.unique(kmeans)
		# fig, axs = plt.subplots(15, 1)
		# fig.tight_layout()
		count = 0
		for idx, col in enumerate(df.columns):
			for idy in range(idx + 1, len(df.columns)):
				plt.scatter(x[y_kmeans == 0, idx], x[ y_kmeans == 0, idy], c="red", marker="s", edgecolor="black", label="Cluster 1")
				plt.scatter(x[y_kmeans == 1, idx], x[ y_kmeans == 1, idy], c="green", marker="s", edgecolor="black", label="Cluster 2")
				plt.scatter(x[y_kmeans == 2, idx], x[ y_kmeans == 2, idy], c="blue", marker="s", edgecolor="black", label="Cluster 3")
				plt.scatter(kmeans.cluster_centers_[:, idx], kmeans.cluster_centers_[:, idy], marker="*" ,s=300, c='yellow', label = 'Centroids')
				plt.xlabel(col)
				plt.ylabel(df.columns[idy])
				plt.legend()
				plt.savefig(f"{df.columns[idy]}VS{col}.png")
				plt.show()
				plt.close()
				
				count +=1
		# print(col, df.columns[idx + 1])
		# plt.show()

	def calculate_matrix(self, df):
		
		X = df.values

		bc_distance_dict = {}
		wc_distance_dict = {}
		for k in [3, 5, 10]:
			if not k in bc_distance_dict:
				bc_distance_dict[k] = 0
			if not k in wc_distance_dict:
				wc_distance_dict[k] = 0
			kmeans = KMeans(n_clusters=k).fit(X)
			pred = kmeans.predict(X)
			centriod_points = kmeans.cluster_centers_

			distance = 0
			# calculate within cluster distance
			for idx, cluster_name in enumerate(np.unique(pred)):
				center = centriod_points[idx]
				data_points = X[pred == cluster_name]
				distance += self.within_cluster_distance(center, data_points)
			wc_distance_dict[k] = distance
			#     wc_distance_dict[k] = kmeans.inertia_
			# calculate between cluster distance
			bc_distance = 0
			for idx in range(kmeans.cluster_centers_.shape[0]):
				for idy in range(idx + 1, kmeans.cluster_centers_.shape[0]):
					bc_distance += self.calculate_distance(kmeans.cluster_centers_[idx], kmeans.cluster_centers_[idy])
			bc_distance_dict[k] = bc_distance

		bcs = pd.Series(bc_distance_dict)
		wcs = pd.Series(wc_distance_dict)
		ratio = {}
		for k in bc_distance_dict.keys():
			ratio[k] =  bc_distance_dict[k] / wc_distance_dict[k]

		ratio = pd.Series(ratio)
		info = pd.DataFrame([bcs, wcs,ratio], index=["BC", "WC", "BC/WC"])
		print(info)
		# print("WC argmin: ", info.loc["WC"].argmin())
		# print("BC argmin: ", info.loc["BC"].argmin() )
		# print("BC/WC argmin: ", info.loc["BC/WC"].argmin() )






Classification()
Cluster()