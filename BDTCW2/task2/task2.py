# importing SparkContext from pyspark
from pyspark import SparkContext

# helper function to get age-group
# this function expect parameter "age"
# this function return age group


def age_group(age):
    age = float(age)
    if age < 10:
        return "0-10"
    elif age < 20:
        return "10-20"
    elif age < 30:
        return "20-30"
    elif age < 40:
        return "30-40"
    elif age < 50:
        return "40-50"
    elif age < 60:
        return "50-60"
    elif age < 70:
        return "60-70"
    elif age < 80:
        return "70-80"

    else:
        return "80+"

# helper function
# this function expect string which is separated by "|"
# this function return parsed data


def parse_with_age_group(data):
    # data = "word1|word2|word3" -- format of data
    user_id, age, gender, occupation, _zip = data.split("|")
    return user_id, age_group(age), gender, occupation, _zip


if __name__ == "__main__":
    # creating a spark context
    sc = SparkContext("local", "pyspark")
    # reading a file
    rdd = sc.textFile("./u.user")
    #  collect method returns all the data in array format
    data_with_age_group = rdd.map(parse_with_age_group)

    # applying filter, we only interested in data of age group 40-50 and 50-60
    # first parameter of filter is lambda function we have to return true or false in this lambda function to keep item or to discard the item
    # this function returns rdd which contain only data of 40-50 and 50-60 age group
    # data_40_50_age_group = data_with_age_group.filter(lambda x: x if x[1] == "40-50" else False)
    data_40_50_age_group = data_with_age_group.filter(
        lambda x: "40-50" in x)

    # data_50_60_age_group = data_with_age_group.filter(lambda x: x if x[1] == "50-60" else False)

    data_50_60_age_group = data_with_age_group.filter(
        lambda x: "50-60" in x)

    # Map a tuple and append int 1 for each occupation
    occupation_40_50 = data_40_50_age_group.map(lambda x: (x[3], 1))

    # Perform aggregation (sum) all the int values for each unique key)
    occupation_40_50_count = occupation_40_50.reduceByKey(lambda x, y: x + y)
    # getting sorted top 10 occupation in descending order
    # by switching key, value and sorting by key
    top_10_occupation_40_50 = occupation_40_50_count.map(
        lambda x: (x[1], x[0])).sortByKey(False).take(10)

    # print("40-50", top_10_occupation_40_50)
    # making rdd from a list
    top_10_occupation_40_50 = sc.parallelize(top_10_occupation_40_50)
    # removing count of occupation we only want occupation
    top_10_occupation_40_50 = top_10_occupation_40_50.map(lambda x: x[1])

    occupation_50_60 = data_50_60_age_group.map(lambda x: (x[3], 1))
    occupation_50_60_count = occupation_50_60.reduceByKey(lambda x, y: x + y)
    top_10_occupation_50_60 = occupation_50_60_count.map(
        lambda x: (x[1], x[0])).sortByKey(False).take(10)
    # print("50-60", top_10_occupation_50_60)
    # making rdd from a list
    top_10_occupation_50_60 = sc.parallelize(top_10_occupation_50_60)
    # top_10_occupation_50_60 = map(lambda x: x[1], top_10_occupation_50_60)
    top_10_occupation_50_60 = top_10_occupation_50_60.map(lambda x: x[1])

    print("Top 10 common occupation in 40-50 and 50-60 age group.")
    # i am using set intersection to find common occupation in 40-50 and 50-60 age group.
    print(top_10_occupation_40_50.intersection(
        top_10_occupation_50_60).collect())
