from sklearn.decomposition import PCA
import numpy as np
x = np.array([[1, 2, 3, 2], [2, 3, 5, 2], [1, 1, 1, 1]]).T

# n_components can be anything less than count of feature attr
pca = PCA(n_components=2)
pca.fit(x)
x_pca = pca.transform(x)
print(x_pca)
print()
x_prime = x - np.mean(x, axis=0)
x_pca_1 = x_prime @ pca.components_.T
print(x_pca_1)
print()
# print(pca.components_)
