import numpy as np
from numpy.core.defchararray import index


def init_population(n_population, phrase_len, random_pool):
    population = []
    # Ascii("a") = 97
    # Ascii("z") = 123
    # ascii of space is 32
    # random_pool = list(range(97, 123))
    # random_pool.extend([32])
    random_string = ""
    for _ in range(n_population):
        temp = [chr(int(np.random.choice(random_pool)))
                for _ in range(phrase_len)]
        random_string = "".join(temp)
        population.append(random_string)
    return population


def compute_fitness_score(population, target):
    fitness_scores = []
    for phrase in population:
        _len = len(phrase)
        score = 0
        for idx in range(_len):
            if phrase[idx] == target[idx]:
                score += 1
        fitness_scores.append(score)
    return fitness_scores


def normalised_fitness_score(fitness_scores, target_len):
    return [fs / target_len for fs in fitness_scores]


def crossover(father, mother):
    _len = len(father)
    mid_point = np.random.randint(low=0, high=_len)
    child = father[0: mid_point] + mother[mid_point:]
    return child


def mutate(child, mutation_rate, random_pool):
    _len = len(child)
    for idx in range(_len):
        random_number = np.random.random(size=1)
        random_number = random_number[0]
        # print("Random Number: ~~~~", random_number)
        if random_number < mutation_rate:
            random_char = chr(np.random.choice(random_pool))
            child = child[0: idx] + random_char + child[idx+1:]
    return child


def evaluate(population, target):
    for idx, phrase in enumerate(population):
        if phrase == target:
            return True, idx
    return False, None


def main(target, n_population=5, mutation_rate=0.01):
    target = target.lower()

    random_pool = list(range(32, 127))
    # random_pool.extend([32])
    generation_count = 0
    while True:
        if generation_count == 0:
            population = init_population(
                n_population=n_population, phrase_len=len(target), random_pool=random_pool)
        fitness_scores = compute_fitness_score(
            population=population, target=target)
        # print(fitness_scores)
        if sum(fitness_scores) == 0:
            population = init_population(
                n_population=n_population, phrase_len=len(target), random_pool=random_pool)
            continue
        fitness_scores = normalised_fitness_score(
            fitness_scores=fitness_scores, target_len=len(target))
        print("Generation: ", generation_count,
              population[np.argmax(fitness_scores)])
        matting_pool = []
        for idx, fs in enumerate(fitness_scores):
            n_repeats = int(fs * 100)
            for _ in range(n_repeats):
                matting_pool.append(population[idx])

        # Now choose mother and father for reproduction
        for idx in range(n_population):
            father_index = np.random.randint(low=0, high=len(matting_pool))
            mother_index = np.random.randint(low=0, high=len(matting_pool))

            father = matting_pool[father_index]
            mother = matting_pool[mother_index]
            # produce a child with cross over
            # mutate a child
            # update existing phrase in population with this child
            child = crossover(father=father, mother=mother)
            child = mutate(child=child, mutation_rate=mutation_rate,
                           random_pool=random_pool)
            population[idx] = child
        break_flag, _index = evaluate(population=population, target=target)
        if break_flag:
            print("Answer Found")
            print(population[_index])
            print(target)
            print("Generation Count: ", generation_count)
            break
        generation_count += 1


if __name__ == "__main__":
    main("to be or not to be that is the question!",
         n_population=5000, mutation_rate=0.01)
