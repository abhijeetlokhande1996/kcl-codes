import numpy as np


def sigmoid(x, deriv=False):
    if deriv:
        return x * (1 - x)
    return 1 / (1 + np.exp(-x))


class NeuralNet(object):
    def __init__(self, input_nodes, hidden_nodes, ouput_nodes, learning_rate):
        self.input_nodes = input_nodes
        self.hidden_nodes = hidden_nodes
        self.output_nodes = ouput_nodes
        self.learning_rate = learning_rate

        # only for ML W7 TUT Q4
        # wih = weight of input to hidden layer
        # wih shape = (2, 2)
        self.wih = np.ones((self.hidden_nodes, self.input_nodes))
        # who = weight of hidden to output layer
        # who shape = (1, 2)
        self.who = np.ones((self.output_nodes, self.hidden_nodes))

    def train(self, x, y):
        # feed forward
        x = x.reshape(x.shape[0], -1)
        y = np.array(y, ndmin=2)
        # @ = numpy dot
        hidden_input = (self.wih @ x)
        hidden_output = sigmoid(hidden_input)
        final_input = hidden_output.T @ self.who.T
        final_output = sigmoid(final_input)

        # back propagation
        # final_error -> error at output node
        # using chain rule, updating a weight of who (who - weights of hidden to output layer)
        final_error = y - final_output
        final_error_delta = final_error * sigmoid(final_output, deriv=True)
        self.who = self.who + self.learning_rate * final_error_delta
        # error at hidden nodes
        hidden_errors = self.who.T @ final_error

        self.wih += self.learning_rate * \
            ((hidden_errors * sigmoid(hidden_output, deriv=True)) @ x.T)

    def query(self, x):
        # feed forward
        x = x.reshape(x.shape[0], -1)
        hidden_input = (self.wih @ x)
        hidden_output = sigmoid(hidden_input)
        final_input = hidden_output.T @ self.who.T
        final_output = sigmoid(final_input)
        if final_output.reshape(-1)[0] > 0.5:
            return 1
        else:
            return 0


nn = NeuralNet(inputnodes=2, hiddennodes=2, ouputnodes=1, learningrate=0.5)
x = np.array([[1, 2], [3, 1], [1, 1], [2, 0]])
y = np.array([1, 0, 1, 0])
for idx, each_x in enumerate(x):
    nn.train(each_x, y[idx])
for idx, each_x in enumerate(x):
    pred = nn.query(each_x)
    print("Pred: ", pred)
