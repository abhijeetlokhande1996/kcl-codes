import pandas as pd
import re
import time
from collections import Counter


def textpreprocessing():
    # Q1
    # Computing All the possible Sentiments that a tweet may have.
    print("The possible sentiments a tweet can have are : \n{}".format(
        dataset.Sentiment.unique()))

    sentiment_counts = dataset.Sentiment.value_counts().sort_values(ascending=False)
    second_popular_value = sentiment_counts[1]
    second_popular_Label = sentiment_counts.index[1]
    print("Second Most popular Sentiment in the Tweet is : \n{} : {} ".format(
        second_popular_Label, second_popular_value))

    # the date with the greatest number of extremely positive tweets
    sentiment_count = dataset.groupby(
        ['Sentiment', 'TweetAt']).size().unstack()
    print("The Date with Maximum Extremely Positive Tweets is:-  ")
    print("Date = {}".format(
        sentiment_count.loc['Extremely Positive'].idxmax()))
    print("Count= {}".format(sentiment_count.loc['Extremely Positive'].max()))

    # Convert the Column OriginalTweet to Lowercase
    dataset['OriginalTweet'] = dataset.OriginalTweet.str.lower()

    # Replace non-alphabetical characters with whitespaces
    # Get Rid of Special Characters and Digits
    dataset["OriginalTweet"] = (dataset["OriginalTweet"].str.lower()).str.replace(
        r"[^a-zA-Z0-9]+", " ", regex=True)

    # Get Rid of Extra white Spaces.
    dataset['OriginalTweet'] = dataset['OriginalTweet'].apply(
        lambda words: ' '.join(words.split()))

    # Q2.
    # Tokenizing the Tweets By using a Lambda function on the Column OriginalTweet.
    s = time.time()
    dataset['OriginalTweet'] = dataset['OriginalTweet'].apply(
        lambda tweets: tweets.split())
    # Count WOrds
    print("Total Word Count:- \n{}".format(
        dataset['OriginalTweet'].apply(lambda tweet: len(tweet)).sum()))

    #
    unique_words = Counter()
    dataset.OriginalTweet
    dataset["OriginalTweet"].apply(unique_words.update)
    #
    """global corpus_appended
    global unique_words
    corpus_appended= []
    qw = dataset.OriginalTweet.apply(lambda x: corpus_appended.extend(x))
    unique_words= set(corpus_appended)"""
    print("Unique words in the entire Corpus: {}".format(len(unique_words)))

    #q = Counter(corpus_appended)
    #print("The 10 Most Frequent Words in the Corpus are: \n{}".format(sorted(q.items(),key=lambda x :x[1],reverse=True)[0:10]))

    # file = open('C:/Users/piyus/Desktop/Kings College/Data Mining/Courseworks/CW-2/data/text_data/english-stop-words-large.txt','r')
    file = open("./english-stop-words-large.txt")
    stopwords = [word.strip() for word in file.readlines()]
    stopwords = list(set(stopwords))
    s = time.time()
    dataset.OriginalTweet = dataset.OriginalTweet.apply(
        lambda tweet: [word for word in tweet if not word in stopwords and not len(word) <= 2])
    print("After: {}".format(time.time()-s))

    print("--:After Text-Preprocessing:--")
    # Count WOrds
    print("Total Word Count:- \n{}".format(
        dataset['OriginalTweet'].apply(lambda tweet: len(tweet)).sum()))

    """corpus_appended= []
    qw = dataset.OriginalTweet.apply(lambda x: corpus_appended.extend(x))
    unique_words= set(corpus_appended)
    print("Unique words in the entire Corpus: {}".format(len(unique_words)))"""
    #q = Counter(corpus_appended)
    #print("The 10 Most Frequent Words in the Corpus are: \n{}".format(sorted(q.items(),key=lambda x :x[1],reverse=True)[0:10]))
    dataset.OriginalTweet = dataset.OriginalTweet.apply(
        lambda x: ' '.join(word for word in x))
    print("Q2 time :{}".format(time.time()-s))

    """#Q3
    t= time.time()
    freq_doc= pd.DataFrame(dataset['OriginalTweet'].str.split().apply(set).tolist()).stack().value_counts()/len(dataset)
    import matplotlib.pyplot as plt
    plt.plot(range(len(freq_doc)),freq_doc)
    plt.xlabel('Words')
    plt.ylabel('Fraction of Documents with word')
    plt.title('Word Frequencies')
    print("Q3 time: {}".format(time.time()-t))
    #plt.show() 
    """
    # 1.4


def model_build():
    from sklearn.feature_extraction.text import CountVectorizer
    c = CountVectorizer(stop_words='english')
    X = c.fit_transform(dataset.OriginalTweet)
    y = dataset.Sentiment
    from sklearn.preprocessing import LabelEncoder
    le = LabelEncoder()
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, dataset.Sentiment)

    from sklearn.naive_bayes import MultinomialNB
    clf = MultinomialNB()
    w = clf.fit(X, y)
    y_pred = w.predict(X_test)
    print("The Training Error for the model is : {}".format(
        1 - clf.score(X_train, y_train)))

    from sklearn.metrics import accuracy_score
    print("The Accuracy of the Prediction is : {}".format(
        accuracy_score(y_test, y_pred)))


def main():
    global dataset
    # dataset = pd.read_csv('C:/Users/piyus/Desktop/Kings College/Data Mining/Courseworks/CW-2/data/text_data/Corona_NLP_train.csv',engine='python')
    dataset = pd.read_csv(
        "./data/text_data/Corona_NLP_train.csv", engine="python")
    start = time.time()
    textpreprocessing()
    model_build()
    cur = time.time()-start
    print("Elapsed Time:{}".format(cur))


if __name__ == "__main__":
    main()
