from skimage.transform import probabilistic_hough_line
import pandas as pd
from collections import Counter
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
import skimage.io
from skimage.color import rgb2gray
from skimage.filters import threshold_otsu, gaussian
from skimage.util import random_noise
# slic - k-means segmentation
from skimage.segmentation import slic
from skimage import feature
from scipy.ndimage import uniform_filter
import requests
import os


class TextMining(object):
    def __init__(self):
        # seaborn set-up
        sns.set()
        file_path = "./data/text_data/Corona_NLP_train.csv"
        # read csv file
        self.corona_nlp = pd.read_csv(file_path, engine="python")
        # fetching stop-words from url
        # transforming it to array of words
        self.stop_words = requests.get(
            "https://raw.githubusercontent.com/fozziethebeat/S-Space/master/data/english-stop-words-large.txt").content.decode('utf−8').split("\n")
        # removing duplicates in stop-words using set
        self.stop_words = set(self.stop_words)
        self.q1()
        self.q2()
        self.q3()
        self.q4()

    def q1(self):
        print("following are the possible sentiments may have")
        # unique method is to find distinct element in the array or series
        print(self.corona_nlp["Sentiment"].unique())
        # calculating frequency of sentiment using value_counts method and then sorting their frequency in descending order
        sentiment_freq = self.corona_nlp["Sentiment"].value_counts(
        ).sort_values(ascending=False)
        print(
            f"second most populat sentiment in tweets {sentiment_freq.index[1]}")

        # filtering the sentiment using  "Extremely Positive"
        # then grouping by "TweetAt", and calculating size using "size" function and arranding in ascending order
        # ep = Extemely Postive Sentiment Data Frame
        ep_df = self.corona_nlp[self.corona_nlp["Sentiment"]
                                == "Extremely Positive"]
        tweet_at_size = ep_df.groupby(
            "TweetAt").size().sort_values(ascending=False)
        print(
            tweet_at_size.index[0], "Date with greatest number of extremely positive tweets.")

        # converting tweets to lower case, replace non-alphabetical characters with white space
        # and ensure that the words of a message are separated by a single whitespace.

        # converting to lower case
        self.corona_nlp["OriginalTweet"] = self.corona_nlp["OriginalTweet"].str.lower()
        # replacing non-alphabetical character with single white space
        self.corona_nlp["OriginalTweet"] = (self.corona_nlp["OriginalTweet"].str.lower()).str.replace(
            r"[^a-zA-Z0-9]+", " ", regex=True)
        # replacing multiple spaces with one space
        self.corona_nlp["OriginalTweet"] = self.corona_nlp["OriginalTweet"].replace(
            "\s+", " ", regex=True)

    def q2(self):
        # tokenise the tweets
        tweets_tokens = self.corona_nlp["OriginalTweet"].str.split()
        print("total number of all words: ", tweets_tokens.str.len().sum())
        # Counter is nothing but dictionary which contain word as a key and count as a value
        # we want word-insensitive count, for this converting all the words in lowercase
        # and for counting, i am calling apply function on top this and passing "unique_words.update" function handler
        # update function works with list just pass the list, it will extract the words from the list
        unique_words = Counter()
        self.corona_nlp["OriginalTweet"].str.lower(
        ).str.split().apply(unique_words.update)
        print("Count of all distinct words: ", len(unique_words.keys()))
        print("Following are the 10 most common words in the corpus")
        # printing 10 most common words using most_common
        print(unique_words.most_common(10))

        # removing stop words and the words having length < 2 from original tweets
        tweets_tokens_without_stopwords = tweets_tokens.apply(lambda _tweets: " ".join(
            [w for w in _tweets if w not in self.stop_words and len(w) > 2]))
        # making a new column in the dataset  and assigning "tweets_tokens_without_stopwords"
        self.corona_nlp["TweetsWithoutStopword"] = tweets_tokens_without_stopwords

        # calculating total number of words in "TweetsWithoutStopword"
        # by converting the column to string then splitting it
        # again converting to string to splitted column and taking a sum.
        count_of_words = self.corona_nlp["TweetsWithoutStopword"].str.split(
        ).str.len().sum()
        print("total number of all words in modified corpus: ", count_of_words)

        # calculating words with frequency using python built-in function Counter
        unique_words = Counter()
        self.corona_nlp["TweetsWithoutStopword"].apply(
            lambda x: unique_words.update(x.split()))

        print("Count of all distinct words in modified corpus: ",
              len(unique_words.keys()))

        print("Following are the 10 most common words in the modified corpus")
        print(unique_words.most_common(10))

    def q3(self):

        # frequency of word according to doc
        # word w present in how many document
        word_freq = pd.DataFrame(self.corona_nlp["TweetsWithoutStopword"].str.split().apply(
            set).tolist()).stack().value_counts()
        # sorting values(freq) in ascending order
        word_freq = word_freq.sort_values(ascending=True)
        word_freq /= len(self.corona_nlp)

        # Linechart

        plt.plot(list(word_freq.index)[-50:], word_freq.values[-50:])
        plt.title("Line chart with word frequency (only last 50 word)")
        plt.xticks(rotation=65)
        fig, ax = plt.subplots(figsize=(18, 7))
        # word_freq.index[:250]
        # creating a line plot
        # x axis of line plot is range of length of series
        # y axis of line plot is frequencies of word
        sns.lineplot(x=range(word_freq.values.shape[0]),
                     y=word_freq.values, ax=ax)
        # setting the title
        ax.set_title("Linechart with word frequencies")
        ax.set_xlabel("Range of word count")
        ax.set_ylabel("Frequency")
        # rotating x-axis label by 65 degree
        plt.xticks(rotation=65)
        plt.show()

        print(
            "I think histogram is useful for deciding the size of the term document matrix")
        print("Including all the terms in the word document frequency matrix is not possible, so i will add 100 most frequent word in the term document matrix")

    def q4(self):
        print("Q4")
        # getting numpy array from series
        corpus = self.corona_nlp["OriginalTweet"].values
        # CountVectorizer is used to create term-document matrix
        # max_feature is a parameter to build a vocabulary that only consider the top max_features ordered by term frequency across the corpus.
        vectorizer = CountVectorizer(max_features=10000)
        # creating term document matrix
        X = vectorizer.fit_transform(corpus)
        # target variable
        y = self.corona_nlp["Sentiment"].values
        clf = MultinomialNB()
        clf.fit(X, y)
        _accuracy = clf.score(X, y)
        print("Accuracy: ", _accuracy * 100)
        print("Error Rate: ", (1 - _accuracy) * 100)

        """
            # splitting the dataset into train and test
            X_train, X_test, y_train, y_test = train_test_split(
                X, y, test_size=0.2)
            # creating the object of NaiveBayes
            clf = MultinomialNB()
            # fitting the training instance with label
            clf.fit(X_train, y_train)
            print("Training Done")
            # calculting the accuracy score on test
            _score = clf.score(X_test, y_test)
            print("Accuracy score on test data: ", _score)
            # calculating the error rate = 1 - accuracy score
            print("Error rate on test data: ", 1 - _score)
        """


class ImageProcessing(object):
    def __init__(self):
        self.op_folder_name = "Output"
        # creating the "output" folder if not exist
        if not os.path.exists(self.op_folder_name):
            os.mkdir(self.op_folder_name)
        self.q1()
        self.q2()
        self.q3()
        self.q4()
        print("All the images are written in 'Output' folder.")

    def q1(self):
        # read the image using skimage
        avenger_img = skimage.io.imread("./data/image_data/avengers_imdb.jpg")
        # size is nothing but row_length * col_length
        print(f"Size of an avenger image is {avenger_img.size}")
        # converting rgb image or color image to gray scale image
        # gray scale contain only one channel and pixel value of gray scale image ranging from 0 to 255
        gray_scale = rgb2gray(avenger_img)
        # for converting gray-scale image binary or black-white image
        # we need to threshold
        # if pixel value is less than threshold then we will replace pixel value with 0 else with 1
        threshold = threshold_otsu(gray_scale)  # input value
        # binary is when less than threshold
        binary_img = (gray_scale < threshold)
        # binary_img = np.where(avenger_img > 128, 255.0, 0.0)

        # saving the image gray-scale image of avenger in the output folder
        plt.imsave(f"{self.op_folder_name}/avenger_gray_scale.png",
                   gray_scale, cmap="gray")
        # saving the binary image of avemger in the output folder
        plt.imsave(f"{self.op_folder_name}/avenger_binary.png",
                   binary_img, cmap="gray")

        # showing the all form of avenger image
        fig, ax = plt.subplots(1, 3)
        fig.set_figwidth(15)
        fig.set_figheight(5)
        ax[0].imshow(avenger_img)
        ax[0].set_title("Avenger original image")
        ax[1].imshow(gray_scale, cmap="gray")
        ax[1].set_title("Avenger grayscale image")
        ax[2].imshow(binary_img, cmap="gray")
        ax[2].set_title("Avenger binary image")

        # plt.show()

    def q2(self):
        # reading bush image using skimage
        bush_house_img = skimage.io.imread(
            "./data/image_data/bush_house_wikipedia.jpg")
        # adding gaussian noise
        # adding noise to original image
        bush_house_noise_added = random_noise(bush_house_img, var=0.1)
        # applying gaussian filter on "bush_house_noise_added" with sigma 1
        bush_house_gaussian_filter = gaussian(
            bush_house_noise_added, sigma=1, multichannel=False)
        # applying uniform filter of size 9*9
        bush_house_g_u_filter = uniform_filter(
            bush_house_gaussian_filter, size=9)
        # plt.imshow(bush_house_g_u_filter)
        # plt.show()

        # plt.imsave(
        #     f"{self.op_folder_name}/bush_house_noise_added.png", bush_house_noise_added)

        # saving bush house with noise and applied gaussian filter
        plt.imsave(f"{self.op_folder_name}/bush_house_noise_added_gaussian_filter_applied.png",
                   bush_house_gaussian_filter, cmap="gray")
        # saving image of uniform filter
        plt.imsave(f"{self.op_folder_name}/bush_house_noise_added_gaussian_filter_uniform_filter_applied.png",
                   bush_house_g_u_filter, cmap="gray")

    def q3(self):
        forestry_commission_img = skimage.io.imread(
            "./data/image_data/forestry_commission_gov_uk.jpg")
        # slic is method from scipy package
        # slic nothing but k-means algorithm
        # n_segment parameter is nothing but k value of k-means algorithm
        five_segments_forest = slic(
            forestry_commission_img, n_segments=5, start_label=1)
        plt.imsave(f"{self.op_folder_name}/five_segment_forest.png",
                   five_segments_forest)

    def q4(self):
        # rolland garros tv5monde.jpg
        rolland_img = skimage.io.imread(
            "./data/image_data/rolland_garros_tv5monde.jpg")
        # converting color image to gray scale
        rolland_gray_scale = rgb2gray(rolland_img)
        # applying canny edge detection algorithm on gray scale image
        rolland_cany = feature.canny(rolland_gray_scale)
        plt.title("Canny Edge Detection")
        plt.imshow(rolland_cany, cmap="gray")
        plt.imsave(f"{self.op_folder_name}/rolland_cany_applied.png",
                   rolland_cany, cmap="gray")
        # Line finding using the Probabilistic Hough Transform
        image = rolland_gray_scale
        edges = feature.canny(image)
        # applying probabilistic_hough transform
        lines = probabilistic_hough_line(edges, threshold=10, line_length=5,
                                         line_gap=3)

        # hough transform.

        fig, ax = plt.subplots(1, 1, figsize=(
            15, 5), sharex=True, sharey=True)
        ax.imshow(edges * 0)
        for line in lines:
            p0, p1 = line
            ax.plot((p0[0], p1[0]), (p0[1], p1[1]))
        ax.set_xlim((0, image.shape[1]))
        ax.set_ylim((image.shape[0], 0))
        ax.set_title('Hough Transform')

        plt.tight_layout()
        # saving the figure
        plt.savefig(
            f"{self.op_folder_name}/rolland_hough_transform.png")
        plt.show()


if __name__ == "__main__":
    TextMining()
    ImageProcessing()
