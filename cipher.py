def caesarCipherEncryptor(string, key):
    # Write your code here.
    if key == 0:
        return string
    _str = string
    ans = ""
    for letter in _str:
        _asc = ord(letter)

        for idx in range(key):
            to_add = 1 
            # print(letter, _asc, to_add)
            if _asc + to_add < 123:
                _asc += to_add
            else:
                _asc = 97
        
        ans += chr(_asc)
    #print(ans)
    return ans
	
ans = caesarCipherEncryptor("abc", 26)
print(ans)

