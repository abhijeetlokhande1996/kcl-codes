import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import datasets
from sklearn.metrics import  accuracy_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import cross_validate, KFold

np.seterr(divide="ignore", invalid="ignore")
"""
age      = 0                              # column indexes in input file
sex      = 1
cp       = 2
trestbps = 3
chol     = 4
fbs      = 5
restecg  = 6
thalach  = 7
exang    = 8
oldpeak  = 9
slope    = 10
ca       = 11
thal     = 12
num      = 13 # this is the thing we are trying to predict
"""

class NaiveBayes:
    def fit(self, X, y):
        n_sample, n_features = X.shape
        self._classes = np.unique(y)
        n_classes = len(self._classes)
        # init mean, variance and priors

        # mean of each input feature associated with each class_labe
        self._mean = np.zeros((n_classes, n_features), dtype=np.float64)
        self._var = np.zeros((n_classes, n_features), dtype=np.float64)
        self._priors = np.zeros(n_classes, dtype=np.float64)
        for idx, c in enumerate(self._classes):
            X_c = X[y==c]
            # column wise mean
            self._mean[idx,: ] = X_c.mean(axis=0)
            self._var[idx,: ] = X_c.var(axis=0)
            # safe side converting deno to float
            self._priors[idx] = X_c.shape[0] / float(n_sample)
     #   print(self._mean[0: 10])
    def predict(self, X):
        y_pred = [self._predict(x) for x in X]
        return np.array(y_pred)
    
    def _predict(self, x):
        posteriors_list = []
        for idx, c in enumerate(self._classes):
            # taking a log because of number exploding
            prior = np.log(self._priors[idx])
            posterior = np.sum(np.log(self._pdf(idx, x)))
            posterior = prior + posterior
            posteriors_list.append(posterior)
        return self._classes[np.argmax(posteriors_list)]

    def _pdf(self, class_idx, x):
        mean = self._mean[class_idx]
        var = self._var[class_idx]
        numeratore = np.exp(-(x - mean)**2 / (2 * var ) )
        denominator = np.sqrt(2 * np.pi * var)
        return numeratore / denominator



def accuracy(y_true, y_pred):
    accuracy = np.sum(y_true == y_pred) / len(y_true)
    return accuracy


X, y = datasets.make_classification(n_samples=1000, n_features=10, n_classes=2,random_state=123)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=123)

nb = NaiveBayes()
nb.fit(X_train, y_train)
predictions = nb.predict(X_test)

print("Naive Bayes classification accuracy", accuracy(y_test, predictions))


df = pd.read_csv("./data.csv", header=None)
cat_col = [ col for col in df.columns if df[col].dtype == "object"]
if cat_col:
    for col in cat_col:
        df[col] = df[col].replace({"?":np.nan})
        df[col] = pd.to_numeric(df[col])

for col, val in df.isna().sum().iteritems():
    if val > 0:
        df[col] = df[col].fillna(df[col].mean())


X = df[df.columns[:-1]]

X = MinMaxScaler().fit_transform(X)
y = df[df.columns[-1]]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
nb = NaiveBayes()
nb.fit(X_train, y_train)
print("Naive Bayes classification accuracy on house price: ", accuracy(y_test ,nb.predict(X_test)))
print("K Fold classification on naive bayes: ")


n_splits = 10
kf = KFold(n_splits=n_splits, shuffle=True)
max_score = None
f_clf = None
for _ in range(2):
    for i in range(n_splits):
        index  = next(kf.split(X), None)
        train_index = list(index[0])
        test_index = list(index[1])
        X_train = X[train_index,:]
     
        y_train = y[train_index]
        X_test = X[test_index,:]
        y_test = y[test_index]
    
        clf = NaiveBayes()
        clf.fit(X_train, y_train)
        score = accuracy(y_test, nb.predict(X_test))
#        print(f"Cross validation at {i} Score: ", score)
        if i == 0:
             max_score = score
             f_clf = clf
        if max_score < score:
            max_score = score
            f_clf = clf
print("Max Score: ", score)
