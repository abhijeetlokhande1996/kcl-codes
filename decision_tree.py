import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import KFold, train_test_split

df = pd.read_csv("./data.csv", header=None)
cat_col = [ col for col in df.columns if df[col].dtype == "object"]
if cat_col:
    for col in cat_col:
        df[col] = df[col].replace({"?":np.nan})
        df[col] = pd.to_numeric(df[col])

for col, val in df.isna().sum().iteritems():
    if val > 0:
        df[col] = df[col].fillna(df[col].mean())

X = df[df.columns[:-1]]
"""
for col in X.columns:
    X[col] = (X[col] - X[col].min()) / (X[col].max() - X[col].min())
"""

X = MinMaxScaler().fit_transform(X)
y = df[df.columns[-1]]
"""
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
"""
n_splits = 10
kf = KFold(n_splits=n_splits, shuffle=True)
max_score = None
f_clf = None
for _ in range(2):
    for i in range(n_splits):
        index  = next(kf.split(X), None)
        train_index = list(index[0])
        test_index = list(index[1])
        X_train = X[train_index,:]
     
        y_train = y[train_index]
        X_test = X[test_index,:]
        y_test = y[test_index]
    
        clf = DecisionTreeClassifier().fit(X_train, y_train)
        score = clf.score(X_test, y_test)
        print(f"Cross validation at {i} Score: ", score)
        if i == 0:
             max_score = score
             f_clf = clf
        if max_score < score:
            max_score = score
            f_clf = clf


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
print("Final Score: ", f_clf.score(X_test, y_test))
