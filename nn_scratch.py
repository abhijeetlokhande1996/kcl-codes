import numpy as np

X = np.array(([2, 9], [1, 5], [3, 6]))
y = np.array([[92], [86], [89]])

"""
    Scaling of X
"""
X = X / np.amax(X, axis=0) 

"""
    np.amax(X, axis=0) calculate max value from each column
"""
"""
    Scaling of y
"""
y = y /100


"""
    Shape of X is (3, 2)
    Shape of Y is (3, 1)
"""
class NeuralNet(object):
    def __init__(self):
        self.input_size = 2
        self.output_size = 1
        self.hidden_size = 3

        self.w1 = np.random.randn(self.input_size, self.hidden_size) # w1 weight in between input layer and hidden layer
        self.w2 = np.random.randn(self.hidden_size, self.output_size) # w2 weight in between hidden layer and output
    
    def sigmoid(self, z , deriv = False):
        if deriv:
            return z * (1 - z)
        return 1 / (1 + np.exp(-z))

    def feed_forward(self, X):
        self.z = np.dot(X, self.w1) # dot product of X(3,2) and w1(2,3)
        self.z2 = self.sigmoid(self.z)
        self.z3 = np.dot(self.z2, self.w2) # dot product of z2 (3,3) and w2 (3,1)
        output = self.sigmoid(self.z3)
        return output

    def back_propagation(self, X, y, output):
        self.output_err = y - output
        self.output_delta = self.output_err * self.sigmoid(output, deriv=True)

        self.z2_error = self.output_delta.dot(self.w2.T)
        self.z2_delta = self.z2_error * self.sigmoid(self.z2, deriv=True)
#        print(X.T.dot(self.z2_delta).shape, self.w1.shape)
        self.w1 += X.T.dot(self.z2_delta)
        self.w2 += self.z2.T.dot(self.output_delta)

if __name__ == "__main__":
    nn = NeuralNet()
    for _ in range(3000):
        op = nn.feed_forward(X)
        nn.back_propagation(X, y, op)
#    print("Loss: ", np.mean(np.square(y - nn.feed_forward(X))) )
    print(nn.feed_forward(X).shape, X.shape)
